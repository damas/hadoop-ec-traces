# Hadoop traces under Erasure Coding

This repository contains traces of Hadoop MapReduce jobs under replication and erasure coding. Sort, Wordcount, and K-means applications are included. These traces are compiled of different software (overlapping and non-overlapping shuffle, disk persistency, failure) and hardware configurations (HDD, SSD, DRAM, 1 Gbps and 10 Gbps network).

Plotting scripts are available in `scripts` directory.

Plotting scripts are written in Python 3.

Required python libraries
+ pyyaml
+ pandas
+ matplotlib

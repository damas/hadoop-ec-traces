#!venv/bin/python
# encoding: utf-8

import os
import sys

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from plot_utils import *


pd.set_option('display.height', 1000)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def is_stats():
    # return True
    return False

PER_CPU = 0


def _plot_cdf_box_helper(df, **kwargs):
    box_plot = kwargs.get("box_plot", False)
    sharey = kwargs.get("sharey", False)
    ax = kwargs.get("ax", None)

    pdf = df.pivot(columns='policy', values='op_throughput')

    if is_stats():
        print(pdf.describe())
        return None

    if box_plot:
        ax = pdf.plot.box(ax=ax, legend=None)
        ax.set_xlabel('')
        ax.set_ylabel('Time (sec)')
    else:
        ax = pdf.plot.hist(ax=ax, sharey=sharey,
                           cumulative=True, density=1, bins=50, histtype='step')
        hatch_cdf_ec_rep(ax)
        ax.set_xlabel('Client operation time (sec)')
        ax.set_ylabel('')
        ax.legend(['EC', 'REP'], title='', loc='upper left', fontsize='xx-large')

    ax.set_title('')
    ax.grid(which='major', linestyle=':')
    return ax


def plot_op_client_cdf(df, **kwargs):

    operation = kwargs['operation']
    nb_clients = kwargs['nb_clients']
    file_size = kwargs['file_size']
    box_plot = kwargs.get("box_plot", False)

    df = df[(df.nb_clients == nb_clients)]
    df = df[(df.file_size == file_size)]
    df = df[(df.operation == operation)]

    if is_stats():
        print("%d %s %s" % (nb_clients, human(file_size), operation))

    ax = _plot_cdf_box_helper(df, box_plot=box_plot)

    if is_stats():
        return None

    return ax.get_figure()


def plot_op_client_cdf_all_clients(df, **kwargs):

    operation = kwargs['operation']
    file_size = kwargs['file_size']
    box_plot = kwargs.get("box_plot", False)

    df = df[(df.file_size == file_size)]
    df = df[(df.operation == operation)]
    df = df[(df.nb_clients > 1)]

    nb_plots = len(df.nb_clients.unique())
    fig, axes = plt.subplots(1,nb_plots, figsize=(nb_plots*5, 8))

    sharey=False
    for ax, nb_clients in zip(axes, sorted(df.nb_clients.unique())):
        dfd = df[(df.nb_clients == nb_clients)].copy()
        dfd['op_time'] = dfd['op_time'] / dfd['op_time'].max()
        ax = _plot_cdf_box_helper(dfd, box_plot=box_plot, ax=ax, sharey=sharey)
        ax.set_ylabel('' if sharey else 'Normalized time (sec)')
        ax.set_ylim(0, 1)
        ax.yaxis.set_major_locator(plt.MultipleLocator(base=.2))
        ax.set_title('nb clients = %d' % nb_clients)
        sharey=True

    return fig


def plot_op_client_cdf_all_sizes(df, **kwargs):

    operation = kwargs['operation']
    nb_clients = kwargs['nb_clients']
    box_plot = kwargs.get("box_plot", False)

    df = df[(df.nb_clients == nb_clients)]
    df = df[(df.operation == operation)]

    nb_plots = len(df.file_size.unique())
    fig, axes = plt.subplots(1,nb_plots, figsize=(nb_plots*5, 8))

    sharey=False
    for ax, file_size in zip(axes, sorted(df.file_size.unique())):
        dfd = df[(df.file_size == file_size)].copy()
        dfd['op_time'] = dfd['op_time'] / dfd['op_time'].max()
        ax = _plot_cdf_box_helper(dfd, box_plot=box_plot, ax=ax, sharey=sharey)
        ax.set_ylabel('' if sharey else 'Normalized time (sec)')
        ax.set_ylim(0, 1)
        ax.yaxis.set_major_locator(plt.MultipleLocator(base=.2))
        ax.set_title('File size = %s' % human(file_size))
        sharey=True

    return fig


def plot_op_client_mean(df, **kwargs):

    as_throughput = kwargs.get("as_throughput", False)

    if as_throughput:
        column_name = 'op_throughput'
        ylabel = 'Throughput (MB/s)'
    else:
        column_name = 'op_time'
        ylabel = 'Time (sec)'

    mean, errors = pivot_df(df, 'file_size', 'policy', column_name)

    if is_stats():
        print("[%s] %d client(s)" % (df.operation.unique()[0],
                                     df.nb_clients.unique()[0]))
        print(mean)
        print(errors)
        print(100*errors/mean)
        return None

    return bar_plot(mean, errors=errors, xticklabels=df.file_size_h.unique(),
                    title='', ylabel=ylabel, xlabel="File size", **kwargs)


def plot_op_file_mean(df, **kwargs):

    as_throughput = kwargs.get("as_throughput", False)
    aggregated = kwargs.get("aggregated", False)

    if as_throughput:
        column_name = 'op_throughput'
        ylabel = 'Throughput (MB/s)'
    else:
        column_name = 'op_time'
        ylabel = 'Time (sec)'

    if aggregated:
        mean, errors = pivot_df_sum(df, 'nb_clients', 'policy', column_name)
        errors = None
        if as_throughput:
            mean /= 1024
            ylabel = 'Throughput (GB/sec)'
    else:
        mean, errors = pivot_df(df, 'nb_clients', 'policy', column_name)

    if is_stats():
        print(mean)
        return None

    return bar_plot(mean, errors=errors,
                    title='', ylabel=ylabel, xlabel="NB of clients")


def plot_heatmap_access(df, **kwargs):

    as_throughput = kwargs.get("as_throughput", False)

    xticklabels = map(human, df["file_size"].unique())

    if as_throughput:
        column_name = 'op_throughput'
        title = 'Throughput per client (MB/s)'
    else:
        column_name = 'op_time'
        title = 'Time per client (sec)'

    df = df.groupby(['file_size', 'nb_clients'])[column_name].mean().unstack(-1)

    ax = sns.heatmap(df.T, cmap='RdYlGn_r', linewidths=0.5,
                        square=True, annot=True, fmt=".2f")
    ax.set_title(title)
    ax.set_ylabel("NB Clients")
    ax.set_xlabel("File size")
    ax.set_xticklabels(xticklabels)
    return ax.get_figure()


#### ---------------------------------------------------------------------------


def access_files_by_client(df, awd, tag):

    def heatmap_access():
        mawd = get_rel_path(awd("heatmap-access"))
        for operation in df.operation.unique():
            # if operation == 'put': continue
            for policy in df.policy.unique():
                dff = df[(df.operation == operation)&(df.policy == policy)]
                save_close(
                    plot_heatmap_access(dff, as_throughput=True),
                    mawd("avg-%s-%s-%s" % (operation, tag, policy)))

    def per_dataset_size():
        for operation in df.operation.unique():
            mawd = get_rel_path(awd("dataset-%s-%s" % (operation, tag)))
            for dataset_size in df.dataset_size.unique():
                if dataset_size != 1024*100:
                    continue
                dff = df[(df.operation == operation)&(df.dataset_size == dataset_size)]
                save_close(
                    plot_op_file_mean(dff, aggregated=True, as_throughput=True),
                    mawd("%s" % (human(dataset_size), )),
                    fontsize=20, format="eps-pdf")

    def op_file_mean():
        for operation in df.operation.unique():
            mawd = get_rel_path(awd("avg-%s-%s" % (operation, tag)))
            for file_size in df.file_size.unique():
                if file_size != 20*1024: continue
                dff = df[(df.operation == operation)&(df.file_size == file_size)]
                save_close(
                    plot_op_file_mean(dff, as_throughput=True),
                    mawd("avg-%s-file" % (human(file_size), )),
                    fontsize=20, format="eps-pdf")

    def op_client_mean():
        for operation in df.operation.unique():
            # if operation == 'get': continue
            mawd = get_rel_path(awd("avg-%s-%s" % (operation, tag)))
            for nb_clients in df.nb_clients.unique():
                if nb_clients != 10: continue
                dff = df[(df.operation == operation)&(df.nb_clients == nb_clients)]
                save_close(
                    plot_op_client_mean(dff, as_throughput=1),
                    mawd("avg-%d-clients" % (nb_clients, )),
                    fontsize=20, format="eps-pdf")
                continue
                save_close(
                    plot_op_client_mean(get_by_ec_cellsize(dff), as_throughput=1, legend_title='Cell size'),
                    mawd("avg-%d-clients-ec-6-3-xM" % (nb_clients, )),
                    fontsize=20, format="eps-pdf")
                save_close(
                    plot_op_client_mean(get_by_ec_schema(dff), as_throughput=1),
                    mawd("avg-%d-clients-ec-x-y" % (nb_clients, )),
                    fontsize=20, format="eps-pdf")

    # plot_type either 'cdf' or 'box'
    def op_client_cdf_box(plot_type):
        for operation in df.operation.unique():
            mawd = get_rel_path(awd("%s-%s-%s" % (plot_type, operation, tag)))
            # for file_size in df.file_size.unique():
                # if file_size != 10240:
                #     continue
                # save_close(
                #     plot_op_client_cdf_all_clients(df,
                #         file_size=file_size,
                #         operation=operation, box_plot=(plot_type=='box')),
                #     mawd("%s-by-nbclients-%s" % (plot_type, human(file_size), )))
            # continue
            for nb_clients in df.nb_clients.unique():
                if nb_clients != 40: continue
                # save_close(
                #     plot_op_client_cdf_all_sizes(df,
                #         nb_clients=nb_clients,
                #         operation=operation, box_plot=(plot_type=='box')),
                #     mawd("%s-by-filesize-%d-clients" % (plot_type, nb_clients, )))
                # continue
                for file_size in df.file_size.unique():
                    if file_size != 1024*5: continue
                    save_close(
                        plot_op_client_cdf(df,
                            nb_clients=nb_clients, file_size=file_size,
                            operation=operation, box_plot=(plot_type=='box')),
                        mawd("%s-%d-clients-%d-MB" % (plot_type, nb_clients, file_size)))

    def as_table():
        for operation in df.operation.unique():
            dfo = df[(df.operation == operation)]
            print(operation)
            print('nb clients,file size,EC,,,,REP')
            print(',',',min,mean,max,stdev'*2)
            for nb_clients in sorted(df.nb_clients.unique()):
                for file_size in sorted(df.file_size.unique()):
                    dft = dfo[(dfo.file_size == file_size)&(dfo.nb_clients == nb_clients)]
                    print(nb_clients, ',', file_size, end='')
                    for policy in ('EC', 'REP'):
                        pp = dft[(dft.policy == policy)]
                        _t = lambda col: getattr(pp['op_throughput'], col)()
                        print(",%.2f,%.2f,%.2f,%.2f" % (
                            _t('min'),_t('mean'),_t('max'),_t('std')), end='')
                    print()


    # df = df[(df.run_id == 0)]

    # heatmap_access()
    # op_file_mean()
    op_client_mean()
    # per_dataset_size()
    # op_client_cdf_box('cdf')
    # op_client_cdf_box('box')
    # as_table()


def access_diff_files_by_client(df, awd):
    access_files_by_client(df, awd, "diff-files")

def access_same_file_by_client(df, awd):
    access_files_by_client(df, awd, "same-file")



def plot_dn_monitoring_cdf(df, **kwargs):

    props = { 'cpu_utilization' : ('CPU utilization %', ),
              'read_count' : ('# disk read op', ),
              'read_bytes' : ('disk read (MB)', ),
              'write_count' : ('# disk write op', ),
              'write_bytes' : ('disk write (MB)', ),
              'bytes_sent' : ('network sent (MB)', ),
              'bytes_recv' : ('network recv (MB)', ),
              'intra' : ('intra (MB)', )}

    datasize = kwargs['datasize']
    nb_clients = kwargs['nb_clients']
    column_name = kwargs['column_name']
    node = kwargs.get("node", None)
    all_nodes = kwargs.get("all_nodes", False)
    ax = kwargs.get("ax", None)

    df = df[(df.datasize == datasize)]
    # df = df[(getattr(df, column_name) != 0)]
    # df = df[(getattr(df, column_name) > 5)]
    if node is not None:
        df = df[(df.node == node)]
        title = str(node)
    else:
        title = "CDF of %s" % props[column_name]
        title = ''

    # if not all_nodes:
    #     df = df.groupby(['timestep', 'policy'])[column_name].sum().reset_index()

    if column_name == 'intra':
        df_c = df[(df.role == 'client')]
        df_dn = df[(df.role == 'datanode')]
        dfcg = df_c.groupby(['timestep', 'policy'])['bytes_sent'].sum().reset_index()
        dfdng = df_dn.groupby(['timestep', 'policy'])['bytes_recv'].sum().reset_index()
        dfdng['intra'] = (dfdng['bytes_recv'] - dfcg['bytes_sent']) / 20
        df = dfdng

    # df = df[(getattr(df, column_name) > 3)]

    if df.empty:
        return ax.get_figure()

    pdf = df.pivot(columns='policy', values=column_name)

    if is_stats():
        print('cluster disk utilization: %s %d - %s' % (
              human(datasize), nb_clients, column_name))
        for policy in df.policy.unique():
            dfp = df[(df.policy == policy)]
            wo_zeros = len(dfp[(getattr(dfp, column_name) > 3)].index)
            all_count = len(dfp.index)
            print('%s %.2f %%' % (policy, 100 * wo_zeros / all_count))
        # len(df.axes[0]), len(df.index), len(df), len(list(df.iterrows()))
        return None

        print('%s %d - %s' % (
              human(datasize), nb_clients, column_name))
        print(pdf.describe())
        return None

    ax = pdf.plot.hist(ax=ax, cumulative=True, density=1, bins=50, histtype='step')
    hatch_cdf_ec_rep(ax)
    ax.set_title(title)
    ax.set_xlabel('Throughput (MB/s)')
    ax.grid(which='major', linestyle=':')
    ax.legend(['EC', 'REP'], title='', loc='upper left', fontsize='xx-large')
    return ax.get_figure()


def plot_dn_monitoring_cdf_per_node(df, **kwargs):

    fig, axes = plt.subplots(4,5, figsize=(30, 24))
    for idx, node in enumerate(sorted(df.node.unique())):
        # print(idx, idx%4,idx%5)
        plot_dn_monitoring_cdf(df, **kwargs,
                                 node=node,
                                 ax=axes[idx%4][idx%5])
    return fig



def plot_magic_square_core(df, prop, **kwargs):

    datasize = kwargs['datasize']
    nb_clients = kwargs['nb_clients']
    ax = kwargs.get("ax", None)

    pdf, _ = pivot_df_sum(df, 'node', 'policy', prop)

    if is_stats():
        print(pdf.describe())
        return None

    box_plot = True

    if box_plot:
        ax = pdf.plot.box(ax=ax, legend=None)
        ax.set_xlabel('')
        ax.set_ylabel('Read data (GB)')
    else:
        ax = pdf.plot.hist(ax=ax, cumulative=True, density=1, bins=50, histtype='step')
        hatch_cdf_ec_rep(ax)
        ax.set_xlabel('Read data (GB)')
        ax.set_ylabel('')
        ax.legend(['EC', 'REP'], title='', loc='upper left', fontsize='xx-large')

    ax.set_title('%d client %s file' % (nb_clients, human(datasize)))
    ax.grid(which='major', linestyle=':')
    return ax


def plot_magic_square_prop(df, prop, **kwargs):

    fig, axes = plt.subplots(5,5, figsize=(30, 30))
    df = df.copy()
    df[prop] /= 1024
    for i, nb_clients in enumerate(sorted(df.nb_clients.unique())):
        dfc = df[(df.nb_clients == nb_clients)]
        for j, datasize in enumerate(sorted(df.datasize.unique())):
            dfd = dfc[(dfc.datasize == datasize)]
            plot_magic_square_core(dfd, prop, **kwargs,
                                     ax=axes[i][j],
                                     nb_clients=nb_clients,
                                     datasize=datasize)
    return fig


def _get_props_monitoring(df, operation, **kwargs):

    node = kwargs.get("node", None)
    cluster = kwargs.get("cluster", False)

    df = df.copy()
    if node is not None:
        df = df[(df.node == node)]

    props = { 'cpu_utilization' : ('CPU utilization %', ),
              'read_count' : ('# disk read op', ),
              'read_bytes' : ('disk read (GB/s)', ),
              'write_count' : ('# disk write op', ),
              'write_bytes' : ('disk write (GB/s)', ),
              'bytes_sent' : ('network sent (GB/s)', ),
              'bytes_recv' : ('network recv (GB/s)', ),
              'intra' : ('intra-network (GB/s)', ),
              'inter' : ('inter-network (GB/s)', ),
              }

    if operation == 'read':
        props = {'read_bytes' : ('disk read (GB/s)', ),
                 'bytes_sent' : ('network sent (GB/s)', )}

    if operation == 'write':
        df['intra'] = df['bytes_sent']
        df['inter'] = df['bytes_recv'] - df['bytes_sent']
        del props['cpu_utilization']

    if cluster:
        for col in props.keys():
            if col in ('cpu_utilization', 'read_count', 'write_count'):
                continue
            # df[col] /= 1024

    return df, props


def analyse_dn_monitoring(df, operation, **kwargs):

    if not is_stats(): return None

    datasize = kwargs.get('datasize', df.datasize.unique()[0])
    nb_clients = kwargs.get('nb_clients', df.nb_clients.unique()[0])
    node = kwargs.get("node", None)
    # cluster = kwargs.get("cluster", False)

    df, props = _get_props_monitoring(df, operation, **kwargs)

    def analyse_connections():
        if node is None: return
        print("node:", node)
        dfp = df[(df.policy == 'rep3')]
        # print(dfp['nb_conn_clients'].unique())
        # print(dfp['nb_conn_clients_unique'].unique())
        print(dfp['nb_conn_clients'].corr(dfp['read_bytes']))
        print(dfp['nb_conn_clients_unique'].corr(dfp['read_bytes']))
        return None
        localaddr = []
        raddr_port = []
        for _, row in dfp.iterrows():
            connections = row['connections']
            print(row['timestep'])
            for conn in connections.split('/'):
                if '0.0.0.0' in conn:
                    continue
                if '172.16.192.15' in conn:
                    continue
                if '127.0.0.1' in conn:
                    continue
                if '172.16.207.' in conn:
                    continue

                raddr, laddr = conn.split(':')
                ip, port = raddr.split('-')
                print(ip, port, row['nb_conn_clients'], row['nb_conn_clients_unique'])

                raddr_port.append(port)
                localaddr.append(laddr)
        # print(df['nb_conn_clients'].describe())
        # print(set(raddr_port))
        return None

    def cpu():
        # _plot(['cpu_SUM','cpu_user','cpu_system','cpu_iowait'], 0, 'CPU %', '')
        print("-- Average CPU [%d clients write %s files]" % (nb_clients, human(datasize)))
        pdf, _ = pivot_df_sum(df[df.cpu_SUM >= 0], 'timestep', 'policy', 'cpu_SUM')
        # print(pdf.describe())
        # print(pdf.sum())
        print(pdf.mean())
        # print(pdf.mean()/20)
        return None

    def write_cluster_level_per_sec():
        print("-- Average cluster disk write throughput (MB/s) [%d clients write %s files]"
                % (nb_clients, human(datasize)))
        pdf, _ = pivot_df_sum(df[df.write_bytes >= 10], 'timestep', 'policy', 'write_bytes')
        # print(df[df.write_bytes > 10].groupby('policy')['timestep'].min())
        print(pdf.describe())
        print('sum:')
        print(pdf.sum())
        # print(pdf.mean())
        # print(pdf.mean()/20)
        return None

    def intra_inter():
        # return None
        for column_name in ('intra', 'inter'):
            dff = df if not True else df[(getattr(df, column_name) != 0)]
            if not len(dff): continue
            print("-- [%s] %d clients write %s files" % (column_name, nb_clients, human(datasize)))
            pdf, _ = pivot_df_sum(dff, 'timestep', 'policy', column_name)
            print(pdf.describe())
            print(pdf.sum())
        return None

    def read_cluster_level_per_sec():
        print("-- Average cluster read throughput (MB/s) [%d clients reading %s files]"
                % (nb_clients, human(datasize)))
        pdf, _ = pivot_df_sum(df[df.read_bytes > 10], 'timestep', 'policy', 'read_bytes')
        print(pdf.describe())
        print(pdf.mean())
        print(pdf.sum())
        # return None

        print("-- Average cluster send throughput (MB/s) [%d clients reading %s files]"
                % (nb_clients, human(datasize)))
        pdf, _ = pivot_df_sum(df[df.bytes_sent > 0], 'timestep', 'policy', 'bytes_sent')
        print(pdf.describe())
        print(pdf.mean())
        print(pdf.sum())
        return None

    def agg_disk_read_active_nodes():
        pdf, _ = pivot_df_sum(df[df.read_count > 0], 'timestep', 'policy', 'read_count')
        print(pdf.describe())
        # return None

        dfz = df[(df.read_bytes != 0)]
        dfz = df[(df.read_count != 0)]
        for policy in df.policy.unique():
            col = dfz[(dfz.policy == policy)]['read_bytes']
            col = dfz[(dfz.policy == policy)]['read_count']
            print(policy)
            print(col.describe())
            # print(col.quantile(0.2))
            # print(col.quantile(0.4))
            # print(col.quantile(0.8))
        return None

    def disk_read_active_nodes():

        print("-- %d clients reading %s files" % (nb_clients, human(datasize)))
        dfp = df[df.read_bytes != 0].groupby(
                    ['node', 'policy'])['read_bytes'].mean().reset_index()
        pdf, _ = pivot_df(dfp, 'node', 'policy', 'read_bytes')
        # print(pdf)
        print(pdf.describe())
        return None

        print(('ec', 'rep3'))
        print(','.join(['min','25%','50%','75%','max']))
        print(human(datasize), end="")
        for policy in ('ec', 'rep3'):
            print("   %.2f" % pdf.mean().xs(policy))
            # print((",%.2f"*5) % (pdf.min().xs(policy),
            # pdf.quantile(0.25).xs(policy), pdf.quantile(0.5).xs(policy),
            # pdf.quantile(0.75).xs(policy), pdf.max().xs(policy))) # , end=""
        print()
        return None

    def misc():
        for policy in df.policy.unique():
            dfp = df[(df.policy == policy)].groupby('node', 'policy')[column_name].mean()
            rs_distr = (1024 * 1024 * dfp['read_bytes'] / dfp['read_count'])
            print("---- [%4s]: request size = %.4f +/- %.2f KB (%d)" % (
                policy, rs_distr.mean(), rs_distr.std(), rs_distr.count()))
        return None

        print("-- %d clients reading %s files" % (nb_clients, human(datasize)))
        for policy in df.policy.unique():
            dfp = df[(df.policy == policy)]
            rs_distr = (1024 * 1024 * dfp['read_bytes'] / dfp['read_count'])
            print("---- [%4s]: request size = %.4f +/- %.2f KB (%d)" % (
                policy, rs_distr.mean(), rs_distr.std(), rs_distr.count()))
        return None
        print("-- %d clients reading %s files" % (nb_clients, human(datasize)))
        for policy in df.policy.unique():
            dfp = df[(df.policy == policy)]
            print("---- [%s] corr('read_bytes','read_count') = %.4f" % (
                  policy, dfp['read_bytes'].corr(dfp['read_count'])))
            print("---- [%s] corr('read_bytes','bytes_sent') = %.4f" % (
                  policy, dfp['read_bytes'].corr(dfp['bytes_sent'])))
        return None
        # print("- %d clients reading %s files: corr('read_bytes','bytes_sent') = %.4f" % (
        #     nb_clients, human(datasize), df['read_bytes'].corr(df['bytes_sent'])))
        print("- %d clients reading %s files: corr('read_bytes','read_count') = %.4f" % (
            nb_clients, human(datasize), df['read_bytes'].corr(df['read_count'])))
        for policy in df.policy.unique():
            dfp = df[(df.policy == policy)]
            print("-- [%s] %d clients reading %s files: corr('read_bytes','read_count') = %.4f" % (
                  policy, nb_clients, human(datasize), dfp['read_bytes'].corr(dfp['read_count'])))
        return None

    def client_side():
        column_name = 'bytes_sent'
        is_client = True
        if is_client:
            propos = ('bytes_sent', )
        else:
            propos = ('bytes_sent', 'bytes_recv', 'write_bytes')
        print("- %s file with %d clients" % (human(datasize), nb_clients, ))
        for column_name in propos:
            dfd = df[(getattr(df, column_name) != 0)]
            dfg = dfd.groupby(['timestep', 'policy'])[column_name].sum().reset_index()
            dfd = dfg.groupby('policy')[column_name].mean() # .reset_index()
            print("%s[%s]\tec:%.2f\trep3:%.2f" % ('(client)' if is_client else '', column_name, 0 \
                if column_name == 'bytes_sent' and not is_client else dfd.xs('ec'), dfd.xs('rep3')))
            # break
        return None


    if operation == 'write':
        write_cluster_level_per_sec()
        intra_inter()
    if operation == 'read':
        # agg_disk_read_active_nodes()
        # disk_read_active_nodes()
        read_cluster_level_per_sec()
        # misc()
    # analyse_connections()
    # client_side()
    # cpu()


def plot_dn_monitoring_policy(df, **kwargs):

    fig, axes = plt.subplots(4,1, figsize=(5+df.timestep.max()//8, 4*4))

    def _plot(cols, index, ylabel, xlabel):
        ax = df.groupby(['timestep'])[cols].mean().plot(ax=axes[index])
        # ax.set_title('')
        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)
        ax.legend(title='', loc='upper left', fontsize='xx-large') # ['EC', 'REP'],
        ax.xaxis.set_major_locator(plt.MultipleLocator(base=10.0))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(base=5.0))
        ax.grid(which='major', linestyle=':')

    _plot(['cpu_SUM','cpu_user','cpu_system','cpu_iowait'], 0, 'CPU %', '')
    _plot(['mem_used','mem_cached'], 1, 'MEM (GB)', '') # 'mem_total',
    _plot(['read_bytes', 'write_bytes'], 2, 'DISK (MB)', '')
    _plot(['bytes_sent', 'bytes_recv'], 3, 'NETWORK (MB)', 'Timestep')

    return fig


def plot_dn_monitoring_per_cpu(df, operation, **kwargs):

    dfc = df.groupby('cpu_id')['cpu_SUM'].max().reset_index()
    dfc = dfc[dfc.cpu_SUM > (40 if operation == 'write' else 20)]
    active_cpus = dfc.cpu_id.unique()
    # print(active_cpus)
    props = ['cpu_SUM','cpu_user','cpu_system','cpu_iowait']
    fig, axes = plt.subplots(len(props),1, figsize=(df.timestep.max()//4, len(props) * 3))
    for i, prop in enumerate(props):
        dfz = df #[df.timestep < 40]
        # dfz = dfz[dfz[prop] != 0]
        dfz = dfz[dfz.cpu_id.isin(active_cpus)]
        mean, errors = pivot_df(dfz, 'timestep', 'cpu_id', prop)
        ax = mean.plot(ax=axes[i], rot=0, legend=None)
        ax.set_title('')
        ax.set_ylabel(prop)
        ax.set_xlabel("Timestep" if i==(len(props)-1) else '')
        if not i:
            ax.legend(title='', ncol=20, loc='upper center',
                        fontsize='large', bbox_to_anchor=(0.5, 1.25))
        ax.xaxis.set_major_locator(plt.MultipleLocator(base=10.0))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(base=5.0))
        ax.grid(which='major', linestyle=':')

    return fig


def plot_dn_monitoring(df, operation, **kwargs):

    datasize = kwargs.get('datasize', df.datasize.unique()[0])
    cluster = kwargs.get("cluster", False)

    df, props = _get_props_monitoring(df, operation, **kwargs)

    fig, axes = plt.subplots(len(props),1, figsize=(16 + (datasize//1024), len(props) * 3))
    for i, prop in enumerate(props.keys()):
        if cluster and prop != 'cpu_utilization':
            mean, errors = pivot_df_sum(df, 'timestep', 'policy', prop)
        else:
            mean, errors = pivot_df(df, 'timestep', 'policy', prop)

        ax = mean.plot(ax=axes[i], rot=0) # yerr=errors , sharex=(i==1),

        ylabel = props[prop][0]
        ax.set_title('')
        ax.set_ylabel(ylabel)
        ax.set_xlabel("Timestep" if i==(len(props.keys())-1) else '')
        ax.legend(title='', loc='upper right', fontsize='xx-large')
        ax.xaxis.set_major_locator(plt.MultipleLocator(base=10.0))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(base=5.0))
        ax.grid(which='major', linestyle=':')

    return fig


def plot_dn_monitoring_write_intra_inter(df, operation, **kwargs):

    if operation == 'read': return None

    datasize = kwargs.get('datasize', df.datasize.unique()[0])
    cluster = kwargs.get("cluster", False)
    ax = kwargs.get("ax", None)

    df, props = _get_props_monitoring(df, operation, **kwargs)

    pivot_func = pivot_df_sum if cluster else pivot_df

    pdf, _ = pivot_func(df, 'timestep', 'policy', 'inter')
    pdf /= 1024
    ax = pdf.plot(ax=ax, rot=0, figsize=None if ax else (10, 3))
    pdf, _ = pivot_func(df[(df.policy == 'REP')], 'timestep', 'policy', 'intra')
    pdf /= 1024
    pdf.plot(ax=ax)

    ax.set_ylabel('Network traffic (GB/s)')
    ax.set_xlabel("Timestep")
    # ax.set_ylim(0, 10)
    ax.legend(['EC inter', 'REP inter', 'REP intra'],
              title='', loc='upper right', fontsize='xx-large')
    ax.xaxis.set_major_locator(plt.MultipleLocator(base=10.0))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(base=5.0))
    ax.grid(which='major', linestyle=':')

    return ax.get_figure()


def plot_dn_monitoring_write_intra_inter_disk(df, operation, **kwargs):

    if operation == 'read': return None
    datasize = kwargs.get('datasize', df.datasize.unique()[0])
    cluster = kwargs.get("cluster", False)

    fig, axes = plt.subplots(2,1, figsize=(16 + (datasize//1024), 2 * 3))
    plot_dn_monitoring_write_intra_inter(df, operation, ax=axes[1], **kwargs)

    df, props = _get_props_monitoring(df, operation, **kwargs)

    pivot_func = pivot_df_sum if cluster else pivot_df

    mean, errors = pivot_func(df, 'timestep', 'policy', 'write_bytes')
    mean /= 1024
    ax = mean.plot(ax=axes[0], rot=0)
    ax.set_ylabel('disk write (GB/s)')
    ax.set_xlabel('')
    ax.legend(['EC', 'REP'], title='', loc='upper right', fontsize='xx-large')
    ax.xaxis.set_major_locator(plt.MultipleLocator(base=10.0))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(base=5.0))
    ax.grid(which='major', linestyle=':')

    return fig


def analyse_cache_hit_on_read(df, **kwargs):

    datasize = kwargs['datasize']
    nb_clients = kwargs['nb_clients']

    dfgr = df[(df.datasize == datasize)].groupby(
            ['policy'])['read_bytes','bytes_sent'].sum()

    for p in ('ec', 'rep3'):
        read_bytes = dfgr.xs(p)['read_bytes']
        bytes_sent = dfgr.xs(p)['bytes_sent']
        cache_hit = 1.0 * bytes_sent / read_bytes
        print("'bytes_sent / read_bytes = %0.2f' for %d %s %s" % (
                cache_hit, nb_clients, human(datasize), p))
    return None


def plot_contributing_dn_cdf(df, **kwargs):

    datasize = kwargs['datasize']
    threshold = kwargs['threshold']
    nb_clients = kwargs['nb_clients']
    contribute_column = kwargs['contribute_column']
    ax = kwargs.get("ax", None)

    df = df[(df.datasize == datasize)].copy()

    df['is_active'] = df[contribute_column].apply(lambda x: int(x > threshold))
    dfgr = df.groupby(['timestep', 'policy'])['is_active'].sum().reset_index()
    if is_stats():
        dfgr = dfgr[(dfgr.is_active != 0)]
    pdf = dfgr.pivot(columns='policy', values='is_active')

    if is_stats():
        print('%s %d - threshold %d (%s)' % (
              human(datasize), nb_clients, threshold, contribute_column))
        print(pdf.describe())
        return None

    ax = pdf.plot.hist(ax=ax, cumulative=True, density=1, bins=50, histtype='step')
    hatch_cdf_ec_rep(ax)
    ax.set_title('Contribution threshold of %d MB' % threshold)
    # ax.set_ylabel('')
    ax.set_xlabel("NB of contributing nodes")
    ax.grid(which='major', linestyle=':')
    ax.legend(['EC', 'REP'], title='', loc='upper left', fontsize='xx-large')
    return ax.get_figure()


def plot_contributing_dn_cdf_list(df, **kwargs):
    fig, axes = plt.subplots(2,2, figsize=(12, 12))
    for idx, threshold in enumerate((25, 50, 75, 100)):
        plot_contributing_dn_cdf(df, **kwargs,
                                 threshold=threshold,
                                 ax=axes[idx//2][idx%2])
    return fig


def plot_contributing_dn_timeline(df, **kwargs):

    datasize = kwargs['datasize']
    threshold = kwargs['threshold']
    nb_clients = kwargs['nb_clients']
    contribute_column = kwargs['contribute_column']

    df = df[(df.datasize == datasize)].copy()

    df['is_active'] = df[contribute_column].apply(lambda x: int(x > threshold))

    df = df.groupby(['timestep', 'policy'])['is_active'].sum().reset_index()
    pdf = df.pivot(index='timestep', columns='policy', values='is_active')

    fig_xlength = df.timestep.unique().size // 10 + 5

    ax = pdf.plot(rot=0, figsize=(fig_xlength, 8))
    ax.set_title('')
    ax.set_ylabel('Number of active DNs')
    ax.set_xlabel("Timestep")
    ax.grid(which='major', linestyle=':')

    # ax.xaxis.set_major_locator(plt.MaxNLocator(fig_xlength, integer=True))
    ax.xaxis.set_major_locator(plt.MultipleLocator(base=10))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(base=5))
    ax.yaxis.set_major_locator(plt.MaxNLocator(20, integer=True))
    ax.legend(['EC', 'REP'], title='', loc='upper left', fontsize='xx-large')
    return ax.get_figure()


def plot_monitoring_prop_box(df, **kwargs):

    sum_df, _ = pivot_df_sum(df, 'node', 'policy', kwargs['propertiy'])
    sum_df /= 1024
    if is_stats():
        print("%d client(s) %s [%s]" % (df.nb_clients.unique()[0],
            human(df.datasize.unique()[0]), kwargs['propertiy']))
        # print(sum_df)
        print(sum_df.sum())
        print(sum_df.describe())
        return None
    ax = sum_df.plot.box(legend=None)
    ax.set_xlabel('')
    ax.set_ylabel('Data (GB)')
    ax.set_title('')
    ax.grid(which='major', linestyle=':')
    return ax.get_figure()


def plot_heatmap_timeline_prop(df, contribute_column, **kwargs):

    # color = kwargs.get('color', 'Reds')
    color = kwargs.get('color', 'RdYlGn_r')

    df = df.copy()
    timesteps_length = df.timestep.unique().size
    df = df[(df.timestep < (timesteps_length-1))]
    df['timestep'] = df['timestep'].apply(lambda x: x-2)
    fig_xlength = timesteps_length // 6 + 3

    df['node'] = df['node'].apply(lambda x: "node %02s" % x.split('-')[1])

    df = df.groupby(['timestep', 'node'])[contribute_column].sum().unstack()
    # df.columns = df.columns.droplevel()

    maxmax = df.max().max()
    vmax = 50 if maxmax < 50 else 100 if maxmax < 100 else 150
    # vmax = None
    xticklabels = 10 if timesteps_length > 100 else 5 if timesteps_length > 20 else \
                    2 if timesteps_length > 10 else 1

    ax = sns.heatmap(df.T, cmap=color, linewidths=0.5, square=True,
                           vmin=0, vmax=vmax, xticklabels=xticklabels)
    ax.set_title('Disk throughput (MB/s)')
    ax.set_ylabel('')
    ax.set_xlabel("Timestep")
    ax.get_figure().set_size_inches(fig_xlength, 5)
    # turn the axis label
    for item in ax.get_yticklabels():
        item.set_rotation(0)
    for item in ax.get_xticklabels():
        item.set_rotation(0)
    return ax.get_figure()


def datanodes_monitoring(df, awd):
    df['datasize'] = df['file_size']

    def __plot_contributing_nodes(dfc, twd, operation, datasize, nb_clients):
        contribute_column = {'read': 'bytes_sent', 'write': 'bytes_recv'}[operation]
        # contribute_column = {'read': 'read_bytes', 'write': 'write_bytes'}[operation]

        tawd = get_rel_path(twd("thresholds-2"))
        save_close(
            plot_contributing_dn_cdf_list(dfc, datasize=datasize, nb_clients=nb_clients,
                                    contribute_column=contribute_column),
            tawd("cdf-%d-clients-%s" % (nb_clients, human(datasize))))

        return
        for threshold in (10, 50, 75, 100):
            tawd = get_rel_path(twd("threshold-%d" % threshold))
            # if datasize != 256: continue

            # save_close(
            #     plot_contributing_dn_timeline(dfc, datasize=datasize, nb_clients=nb_clients,
            #                             threshold=threshold, contribute_column='bytes_sent'),
            #     tawd("contributing-node-%s-%d-client-%d" % (
            #             human(datasize), nb_clients, threshold)))
            save_close(
                plot_contributing_dn_cdf(dfc, datasize=datasize, nb_clients=nb_clients,
                                        threshold=threshold, contribute_column=contribute_column),
                tawd("cdf-%d-clients-%s" % (nb_clients, human(datasize))))
            return


    def _plot_by_operation_and_role(df, operation, role):
        twd = get_rel_path(awd("%s/%s" % (role, operation)))

        operation_columns = {   'read': ('bytes_sent', 'read_bytes'),
                                'write': ('bytes_recv', 'write_bytes')}
        disk_operation_columns = {  'read': ('read_bytes', ),
                                    'write': ('write_bytes', )}
        # disk_operation_columns = {  'read': ('bytes_sent', ),
        #                             'write': ('write_bytes', )}

        # disk_operation_columns = {  'read': ('nb_conn_clients', )}
        # disk_operation_columns = {  'read': ('nb_conn_clients_unique', )}

        def magic_square_prop():
            prop='read_bytes'
            save_close(
                plot_magic_square_prop(df, prop),
                twd("all-%s" % (prop,)))

        def monitoring_box():
            cwd = get_rel_path(twd('dn-monitoring-box'))
            for nb_clients in sorted(df.nb_clients.unique()):
                if nb_clients != 40: continue
                for datasize in sorted(df.datasize.unique()):
                    # if datasize != 10240*2: continue
                    dfd = df[(df.nb_clients == nb_clients)&(df.datasize == datasize)]
                    for prop in operation_columns[operation]:
                        # if prop == 'read_bytes': continue
                        save_close(
                            plot_monitoring_prop_box(dfd, propertiy=prop),
                            cwd("%s-%d-clients-%s" % (prop.replace('_','-'), nb_clients, human(datasize), )))

        def heatmap_timeline():
            contribute_column = disk_operation_columns[operation][0]
            cwd = get_rel_path(twd("%s-heatmap" % contribute_column))
            for nb_clients in sorted(df.nb_clients.unique()):
                if nb_clients != 5: continue
                for datasize in sorted(df.datasize.unique())[::-1]:
                    if datasize != 1024*5: continue
                    dfd = df[(df.nb_clients == nb_clients)&(df.datasize == datasize)]
                    for policy in df.policy.unique(): #'ec',
                    # for policy in ('ec', 'rep3'): #'ec',
                        dfp = dfd[dfd.policy == policy]
                        save_close(
                            plot_heatmap_timeline_prop(dfp, contribute_column),
                            cwd("%s-%d-clients-%s" % (
                                    policy, nb_clients, human(datasize))),
                            ticks_fontsize=12, format="eps")
                        # for color in Colormap:
                        #     save_close(
                        #         plot_heatmap_timeline_prop(dfd, contribute_column,
                        #             policy=policy, datasize=datasize, nb_clients=nb_clients, color=color),
                        #         cwd("%s-%d-clients-%s-%s" % (
                        #                 policy, nb_clients, human(datasize), color)),
                        #         ticks_fontsize=12)
                        # print(policy, nb_clients, human(datasize))
                        # return

        def monitoring_cdf():
            for nb_clients in sorted(df.nb_clients.unique()):
                if nb_clients != 40: continue
                for datasize in sorted(df.datasize.unique()):
                    if datasize != 1024*20: continue
                    dfd = df[(df.nb_clients == nb_clients)&(df.datasize == datasize)]
                    for column_name in operation_columns[operation]:
                        cwd = get_rel_path(twd("cdf-%s-with-zeros" % column_name))
                        # save_close(
                        #     plot_dn_monitoring_cdf_per_node(dfc,
                        #         datasize=datasize, nb_clients=nb_clients, column_name=column_name),
                        #     cwd("cdf-%d-clients-%s-per-node" % (nb_clients, human(datasize))))
                        save_close(
                            plot_dn_monitoring_cdf(dfd, all_nodes=True,
                                datasize=datasize, nb_clients=nb_clients, column_name=column_name),
                            cwd("cdf-%d-clients-%s-per-node" % (nb_clients, human(datasize))),
                            fontsize=20, format="eps")
                        continue
                        # nwd = get_rel_path(cwd("cdf-%d-clients-%s" % (nb_clients, human(datasize))))
                        # for node in dfc.node.unique():
                        #     save_close(
                        #         plot_dn_monitoring_cdf(dfc, datasize=datasize,
                        #         nb_clients=nb_clients, column_name=column_name, node=node),
                        #         nwd("node-%s" % (node, )))

        def monitoring_timeline():
            cwd = get_rel_path(twd('monitoring-timeline'))
            c1wd = get_rel_path(cwd('avg-node'))
            c2wd = get_rel_path(cwd('cluster'))
            c3wd = get_rel_path(cwd('per-node'))
            for nb_clients in sorted(df.nb_clients.unique()):
                if nb_clients != 40: continue
                for datasize in sorted(df.datasize.unique()):
                    if datasize != 1024*5: continue
                    # if datasize == 256: continue
                    dfd = df[(df.nb_clients == nb_clients)&(df.datasize == datasize)]
                    # analyse_dn_monitoring(dfd, operation, cluster=True)
                    # continue
                    dname = "%d-clients-%s" % (nb_clients, human(datasize))

                    # for policy in df.policy.unique():
                    #     if not PER_CPU: break
                    #     pmawd = get_rel_path(c1wd(policy))
                    #     dfp = dfd[(dfd.policy == policy)]
                    #     save_close(
                    #         plot_dn_monitoring_per_cpu(dfp, operation),
                    #         pmawd("%s-per-cpu" % dname))
                    #     continue
                    #     nwd = get_rel_path(pmawd(dname))
                    #     for node in sorted(dfd.node.unique()):
                    #         dfpn = dfp[(dfp.node == node)]
                    #         save_close(
                    #             plot_dn_monitoring_per_cpu(dfpn, operation),
                    #             nwd("node-%s" % (node, )))
                    #
                    # continue
                    # for policy in df.policy.unique():
                    #     pmawd = get_rel_path(c1wd(policy))
                    #     dfp = dfd[(dfd.policy == policy)]
                    #     save_close(
                    #         plot_dn_monitoring_policy(dfp),
                    #         pmawd("%s" % dname))
                    #     continue
                    #     nwd = get_rel_path(pmawd(dname))
                    #     for node in sorted(dfd.node.unique()):
                    #         save_close(
                    #             plot_dn_monitoring_policy(dfp[(dfp.node == node)]),
                    #             nwd("node-%s" % (node, )))
                    #
                    # continue
                    #
                    #
                    save_close(
                        plot_dn_monitoring_write_intra_inter_disk(dfd, operation, cluster=True,
                            datasize=datasize, nb_clients=nb_clients),
                        c2wd("%s-write-disk" % dname), format="eps")
                    continue
                    # save_close(
                    #     plot_dn_monitoring_write_intra_inter(dfd, operation, cluster=True,
                    #         datasize=datasize, nb_clients=nb_clients),
                    #     c2wd("%s-write" % dname))
                    # continue

                    save_close(
                        plot_dn_monitoring(dfd, operation, cluster=True,
                            datasize=datasize, nb_clients=nb_clients),
                        c2wd("%s" % dname))
                    continue
                    save_close(
                        plot_dn_monitoring(dfd, operation,
                            datasize=datasize, nb_clients=nb_clients),
                        c1wd("%s" % dname))
                    continue
                    pwd = get_rel_path(c3wd(dname))
                    for node in dfd.node.unique():
                        save_close(
                            plot_dn_monitoring(dfd, operation, node=node,
                                datasize=datasize, nb_clients=nb_clients),
                            pwd("node-%s" % (node, )))
                        return

        def contributing_nodes():
            if operation != 'read' or role != 'datanodes':
                return
            for nb_clients in sorted(df.nb_clients.unique()):
                # if nb_clients != 1: continue
                dfc = df[(df.nb_clients == nb_clients)]
                for datasize in sorted(df.datasize.unique()):
                    # if datasize != 10240*2: continue
                    # if datasize < 11000: continue
                    __plot_contributing_nodes(dfc, twd, operation, datasize, nb_clients)

        def cache_hit_on_read():
            if operation != 'read' or role != 'datanodes':
                return
            for nb_clients in sorted(df.nb_clients.unique()):
                # if nb_clients != 1: continue
                for datasize in sorted(df.datasize.unique()):
                    dfd = df[(df.nb_clients == nb_clients)&(df.datasize == datasize)]
                    analyse_cache_hit_on_read(dfd, datasize=datasize, nb_clients=nb_clients)

        monitoring_box()
        monitoring_cdf()
        monitoring_timeline()
        heatmap_timeline()
        contributing_nodes()
        cache_hit_on_read()
        # magic_square_prop()

    def _plot_by_role(df, role):
        for operation in df.operation.unique():
            if operation == 'write': continue
            print('operation', operation)
            _plot_by_operation_and_role(df[(df.operation == operation)], operation, role)

    df['role'] = df['node'].apply(lambda x:
        'client' if "ecotype" in x else 'datanode')

    if 'cpu_id' in list(df):
        df['timestep'] = df['timestep'].apply(lambda x: x//9)
        df = df[(df.timestep > 1)]
        if not PER_CPU:
            df = df[(df.cpu_id == 0)]

    df_dn = df[df['node'].str.contains("econome")]
    df_client = df[df['node'].str.contains("ecotype")]

    # if 'cpu_id' in list(df):
    #     df_client = df_client.copy()
    #     df_client['timestep'] = df_client['timestep'].apply(lambda x: x//40)
    #     df_client = df_client[(df_client.timestep > 1)]
    #     df_dn = df_dn.copy()
    #     df_dn['timestep'] = df_dn['timestep'].apply(lambda x: x//9)
    #     df_dn = df_dn[(df_dn.timestep > 1)]
    #     df_dn = df_dn[(df_dn.cpu_id == 0)]

    _plot_by_role(df_dn, "datanodes")
    # _plot_by_role(df_client, "clients")
    # _plot_by_role(df, "datanodes")

    # dfg = df.groupby(['role', 'operation', 'datasize', 'policy', 'nb_clients'])
    # df1 = dfg.time.max() - dfg.time.min()
    # df2 = dfg.timestep.max() - dfg.timestep.min()
    # dfdiff = df1 - df2
    # dff = (dfdiff / df1).reset_index()
    # print(dff[0].describe())


def datanodes_monitoring_distinct_files(df, awd):
    print("Access distinct files [Monitoring]")
    datanodes_monitoring(df, get_rel_path(awd("monitoring-diff-files")))

def datanodes_monitoring_same_file(df, awd):
    print("Access same file [Monitoring]")
    datanodes_monitoring(df, get_rel_path(awd("monitoring-same-file")))


def plot_dn_load(df, **kwargs):
    sorted_df = df.sort_values(by=['data_load'])
    ax = sorted_df.plot.bar(x='node', y='data_load', legend=None)
    ax.set_ylabel('Server load (GB)')
    ax.set_xlabel('Servers sorted by load')
    ax.grid(which='major', linestyle=':')
    ax.xaxis.set_major_locator(plt.NullLocator())
    for bar in ax.patches:
        bar.set_color('grey')
    return ax.get_figure()

def plot_dn_load_metric(df, **kwargs):
    title = ''
    ylabel = kwargs.get("desc", "")
    mean, errors = pivot_df(df, 'total_logical', 'policy', 'data_load')
    if is_stats():
        print(ylabel)
        print(df.sort_values(by=['total_logical', 'policy', 'nb_files']))
        return None
    return bar_plot(mean, title=title, ylabel=ylabel, legend_loc='upper right',
                          xlabel="Total logical datasize (GB)")


def plot_dn_load_per_inc_files(df, **kwargs):
    ylabel = kwargs.get("desc", "")
    xlabel = "NB of files"
    mean, _ = pivot_df(df, 'nb_files', 'policy', 'data_load')
    if is_stats():
        # print(ylabel)
        # print(df.sort_values(by=['total_logical', 'policy', 'nb_files']))
        return None
    return bar_plot(mean, title=title, ylabel=ylabel, xlabel="NB of files")


def plot_dn_load_per_inc_files_all(df, **kwargs):
    mean, _ = pivot_df(df, 'total_logical', 'policy_filesize', 'data_load')
    mean = mean.dropna()
    ax = mean.plot(rot=0)#, style=['r*-','bo-','r^-','b^-']
    ax.set_ylim(0, ax.get_ylim()[1])
    ax.legend(title='', loc='upper right', fontsize='xx-large')
    ax.set_ylabel(kwargs.get("desc", ""))
    ax.set_xlabel("Data size (GB)")
    ax.grid(which='major', linestyle=':')
    for line, ls in zip(ax.lines, [':', '--', ':', '--']):
        line.set_linestyle(ls)
    return ax.get_figure()


def dn_data_load(df, awd):
    print("DataNodes data load")

    def compute_loadbalance(df, func):
        return df.groupby(['policy', 'total_logical', 'policy_filesize', 'file_size', 'nb_files']
                         )['data_load'].apply(func).reset_index()

    for func, desc in [#(percent_imbalance, "percent imbalance"),
                       (cv, "Coefficient of variation")]:
        vwd = get_rel_path(awd(str(desc.lower().replace(' ', '-'))))
        # df_func = compute_loadbalance(df[df.total_logical <= 100000], func)
        # save_close(
        #     plot_dn_load_metric(df_func, desc=desc),
        #     vwd("lessthan10"))
        # continue

        df_func = compute_loadbalance(df, func)
        save_close(
            plot_dn_load_per_inc_files_all(df_func, desc=desc),
            vwd("load_with_inc_dataset_size"), format="eps")

        # for df_func_f, file_size in itr_df_props(df_func, ['file_size'], None):
        #     save_close(
        #         plot_dn_load_per_inc_files(df_func_f, desc=desc),
        #         vwd("%s" % (human(file_size), )))
        # for df_func_f, nb_files in itr_df_props(df_func, ['nb_files'], None):
        #     save_close(
        #         plot_dn_load_metric(df_func_f, desc=desc),
        #         vwd("%d-files" % (nb_files, )))

    return

    for dfd, nb_files, file_size, policy in itr_df_props(df,
            ['nb_files', 'file_size', 'policy'],
            [lambda cl: cl != 40, lambda ds: ds != 10240, lambda x: 0]):
        save_close(
            plot_dn_load(dfd, ),
            awd("%d-files-%s-%s" % (nb_files, human(file_size), policy)))


def blocks_distribution(df, awd):
    print("EC blocks distribution")

    def parse_filepath(hdfspath):
        size, nb_files, filename_host = hdfspath[6:].split('/')
        return 10*(int(nb_files)+1), int(size)

    df['policy'] = 'ec'
    df[['nb_files', 'file_size_mb']] = df['filename'].apply(
        lambda hdfspath: pd.Series(parse_filepath(hdfspath)))

    df['file_size'] = df['file_size_mb'] * 1024**2
    df = df[df.file_size_mb > 1000].copy()

    df["policy_filesize"] = df["file_size_mb"].apply(lambda fs:
                            "Big files (" if fs > 1000 else "Small files ("
                            ) + df["policy"].apply(lambda x: x+")")

    df['total_logical'] = df['file_size_mb'] * df['nb_files']
    df['total_logical'] //= 1024
    r_factor = {'ec': 1.5, 'rep3': 3}
    df['total_physical'] = df['total_logical'] * df['policy'].apply(lambda x: r_factor[x])

    dfall = df.copy()
    dfall["policy_filesize"] = 'Total chunks'

    df = df[df.type == 'data'].copy()
    # df = df[df.type == 'parity'].copy()
    df["policy_filesize"] = 'Data chunks'

    df = pd.concat([dfall, df])

    df.drop(['filename', 'offset'], axis=1, inplace=True)

    df['nb_blocks'] = 1
    # for each iteration, count the nb of blocks per host
    df = df.groupby(['policy', 'total_logical', 'policy_filesize', 'file_size',
                     'nb_files', 'host'])['nb_blocks'].count().reset_index()

    # cumsum of blocks per host
    df['nb_blocks'] = df.groupby(
        ['policy', 'policy_filesize', 'file_size', 'host']
        )['nb_blocks'].cumsum().reset_index()['nb_blocks']

    df['data_load'] = df['nb_blocks'] * 256

    dn_data_load(df, awd)



def hdfs_3x(input_dir, output_dir):

    awd = get_rel_path(output_dir)
    rwd = get_rel_path(input_dir)

    for csv_filename, plot_func, read_csv_func in [
        ('concurrent_op_distinct_files.csv', access_diff_files_by_client, read_hdfs_csv),
        ('concurrent_op_same_file.csv', access_same_file_by_client, read_hdfs_csv),
        # ('paralle_op_diff_files_by_client.csv', access_diff_files_by_client, read_hdfs_csv),
        # ('paralle_op_same_file_by_client.csv', access_same_file_by_client, read_hdfs_csv),
        # ('dn_data_load.csv', dn_data_load, read_data_load_csv),
        # ('blocks.csv', blocks_distribution, read_blocks_csv),
        # ('monitoring-same-file.csv.gz', datanodes_monitoring_same_file, read_monitoring_csv),
        # ('monitoring-diff-files.csv.gz', datanodes_monitoring_distinct_files, read_monitoring_csv),
        # ('monitoring-distinct-files.csv.gz', datanodes_monitoring_distinct_files, read_monitoring_csv),
    ]:
        csv_path = rwd(csv_filename)
        if os.path.exists(csv_path):
            df = read_csv_func(csv_path)
            plot_func(df, awd)
        else:
            print('skipped:', csv_filename)


if __name__ == "__main__":

    if len(sys.argv) != 3:
        print('usage: ./plot_hdfs.py input-dir output-dir')
        sys.exit(1)

    input_dir, output_dir = sys.argv[1:]

    hdfs_3x(input_dir, output_dir)

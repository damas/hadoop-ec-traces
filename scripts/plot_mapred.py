#!venv/bin/python
# encoding: utf-8

import os
import sys

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sns

from matplotlib.collections import LineCollection
from matplotlib.transforms import Affine2D

from plot_utils import *


IS_STATS = 0

IS_MULTIJOBS = 0


def reglage_datasize(x):
    ds, p = x[0], x[1]
    if p == '32M' or p.endswith('32768k'):
        return {80:40 , 40:20 , 20: 10, 60: 40, 30: 20, 10:10}[ds]
    if (p == '08M' or p.endswith('8192k')) and ds == 50:
        return 40
    return ds


def filter_df(df):
    return df
    policy = None
    job_name = None
    input_size = None
    datasize = None
    run_id = None

    # run_id = 0
    # policy = 'EC'
    job_name = 'sorter'
    # job_name = 'word-count'
    # job_name = 'kmeans'
    # datasize = 40
    # input_size = datasize

    def filter_prop(df, col, val):
        return df[df[col] == val] if (val is not None) and (col in df.columns) else df
        if hasattr(df, col):
            df_prop = getattr(df, col)
            return df[df_prop == val] if val is not None else df
        return df

    df = filter_prop(df, 'run_id', run_id)
    df = filter_prop(df, 'job_name', job_name)
    df = filter_prop(df, 'policy', policy)
    df = filter_prop(df, 'datasize', datasize)
    # df = filter_prop(df, 'input_size', input_size)

    return df


def extract_phase(df, filter_df_func):
    assert len(df.job_name.unique()) == 1
    assert len(df.run_id.unique()) == 1
    job_name = df.job_name.unique()[0]
    run_id = df.run_id.unique()[0]
    # print(job_name, run_id)
    dft = df_jobs[(df_jobs.job_name == job_name) &
                  (df_jobs.run_id == run_id)]
    # print(dft.datasize.unique(), dft.policy.unique())
    # print(df.datasize.unique(), df.policy.unique())

    for d in df.datasize.unique():
        for p in df.policy.unique():
            # print(d, p)
            dfj = dft[(dft.policy == p) & (dft.datasize == d)]
            # print(dfj)
            assert len(dfj) == 1, dfj
            df = filter_df_func(df, d, p, dfj)
    return df


def extract_phase_x(df, job_name, filter_df_func):
    assert len(df.run_id.unique()) == 1
    run_id = df.run_id.unique()[0]
    print(job_name, run_id)
    dft = df_kmeans[(df_kmeans.job_name == job_name) &
                    (df_kmeans.run_id == run_id)]
    for p in df.policy.unique():
        dfj = dft[dft.policy == p]
        # print(dfj)
        assert len(dfj) == 1, dfj
        df = filter_df_func(df, p, dfj)
    return df


def extract_kmeans_job(df, jobname):
    print('-- extract_kmeans_job')
    def filter_df_func(df, p, dfj):
        job_start_t = dfj.launchedAt.values[0]
        job_finish_t = dfj.finishedAt.values[0]
        print('kmeans', jobname, p, job_finish_t-job_start_t)
        return df[~((df.policy == p) & \
                  ((df.time < job_start_t) | (df.time > job_finish_t)))]
    return extract_phase_x(df, jobname, filter_df_func)


def extract_map_phase(df):
    print('-- MAP PHASE')
    def filter_df_func(df, d, p, dfj):
        job_start_t = dfj.launchedAt.values[0]
        map_finish_t = dfj.reduce_startTime.values[0]
        # print('map', d, p, map_finish_t-job_start_t)
        return df[~((df.datasize == d) & (df.policy == p) & \
                  ((df.time < job_start_t) | (df.time > map_finish_t)))]
    return extract_phase(df, filter_df_func)


def extract_reduce_stage(df):
    print('-- REDUCE STAGE')
    def filter_df_func(df, d, p, dfj):
        dfa = df_attempts[(df_attempts.job_name == dfj.job_name.unique()[0]) &
                          (df_attempts.run_id == dfj.run_id.unique()[0]) &
                          (df_attempts.policy == p) & (df_attempts.input_size == d) &
                          (df_attempts.type == "reduce")]
        red_stage_start_t = dfa['sortFinished'].quantile(0.1)
        job_finish_t = dfj.finishedAt.values[0]
        # print('red', d, p, job_finish_t-red_stage_start_t)
        # print(int(mid_sort_time))
        return df[~((df.datasize == d) & (df.policy == p) & \
                  ((df.time < red_stage_start_t) | (df.time > job_finish_t)))]
    return extract_phase(df, filter_df_func)

def extract_reduce_phase(df):
    print('-- REDUCE PHASE')
    def filter_df_func(df, d, p, dfj):
        map_finish_t = dfj.reduce_startTime.values[0]
        job_finish_t = dfj.finishedAt.values[0]
        # print('red', d, p, job_finish_t-map_finish_t)
        return df[~((df.datasize == d) & (df.policy == p) & \
                  ((df.time < map_finish_t) | (df.time > job_finish_t)))]
    return extract_phase(df, filter_df_func)


def is_hibench_jobs(df):
    sample_jpn_names = ['Create pagerank nodes', 'autogen-7.1-SNAPSHOT-jar-with-dependencies.jar']
    return set(sample_jpn_names) & set(df.job_name.unique())

def plot_hibench_jobs(df, awd, plotfunc):

    if plotfunc == mapred_jobs_per_job:
        df['hdfs_bytes_read'] /= ONE_GB
        df['hdfs_bytes_written'] /= ONE_GB

    def hibench_jobnames(x):
        if x.startswith('Cluster Classification'):
            return 'c'
            return 'classification'
        if x.startswith('Cluster Iterator'):
            return 'I-%02d' % int(x.split()[4])
            return 'Itr-%02d' % int(x.split()[4])
        if x.startswith('Create'):
            return x.replace(" pagerank ", '\n')
        # if x == 'Create pagerank links':
        #     return 'Create\nlinks'
        # if x == 'Create pagerank nodes':
        #     return 'Create\nnodes'
        if x.startswith('Pagerank_Stage'):
            p = x[len('Pagerank_'):].split('-')
            return "(itr:%s)\n%s" % tuple(p[::-1])
        return x

    global df_kmeans
    df['job_name'] = df['job_name'].apply(hibench_jobnames)
    df_pr = df[df['job_name'].str.startswith('(')].copy()
    df_kmeans = df[df['job_name'].str.startswith(('I', 'c'))].copy()
    # df_kmeans = df[df['job_name'].str.startswith(('Itr', 'classification'))].copy()

    if plotfunc is None:
        return

    def printdf(df):
        print(df.groupby(['policy', 'job_name', 'run_id'])['execution_time', 'hdfs_bytes_read', 'hdfs_bytes_written'].sum().round(2))
        print(df[['policy', 'job_name', 'run_id', 'execution_time', 'hdfs_bytes_read', 'hdfs_bytes_written']].round(2))

    # printdf(df_pr)
    # printdf(df_kmeans)

    # plotfunc(df_kmeans, awd('kmeans/all_jobs'))
    # plotfunc(df_pr, awd('pagerank/all_jobs'))
    # plotfunc(df_pr[df_pr['job_name'] == '(itr:1)\nStage1'], awd('pagerank/Stage1-1'))
    # plotfunc(df_pr[df_pr['job_name'] == '(itr:1)\nStage2'], awd('pagerank/Stage2-1'))

    # plotfunc(df_kmeans[df_kmeans['job_name'] == 'classification'], awd('kmeans/classification'))
    plotfunc(df_kmeans[df_kmeans['job_name'] == 'I-01'], awd('kmeans/Itr-01'))
    # plotfunc(df_kmeans[df_kmeans['job_name'] == 'Itr-02'], awd('kmeans/Itr-02'))


#
# Meta mapred analysis
#

def _iterate_datasize_policy(df):
    for datasize in df.datasize.unique():
        dd = df[(df.datasize == datasize)]
        for policy in df.policy.unique():
            pp = dd[(dd.policy == policy)]
            yield datasize, policy, pp

def _iterate_datasize_policy_2(df1, df2):
    for datasize in df1.datasize.unique():
        for policy in df1.policy.unique():
            df1x = df1[(df1.datasize == datasize) & (df1.policy == policy)]
            df2x = df2[(df2.datasize == datasize) & (df2.policy == policy)]
            yield datasize, policy, df1x, df2x


def mapred_failures_exchanged_data(csv_path, awd):
    print ("Failures exchanged data")
    df = read_exchanged_data_csv(csv_path)
    df = filter_df(df)

    def failure_impact_prop(df, col):
        _t = lambda s: getattr(s, col).values[0]
        dft = df.groupby(['datasize', 'policy', 'nb_failures']
                        )[col].mean().reset_index()
        print("-- failure impact (%s)" % col)
        for datasize, policy, dd in _iterate_datasize_policy(dft):
            v0f = _t(dd[(dd.nb_failures == 0)])
            v1f = _t(dd[(dd.nb_failures == 1)])
            ratio = 100 * (v1f-v0f) / v1f
            print(datasize, policy, round(ratio, 2), '%', 'more data overhead')

    def failure_impact(df):
        failure_impact_prop(df, 'tx')

    for job_name in df.job_name.unique():
        print("%s%s%s" % (RED, job_name, NC))
        dff = df[(df.job_name == job_name)]
        failure_impact(dff)


def plot_execution_time_by_failure(df, **kwargs):
    mean, errors = pivot_df(df, 'failure', 'policy', 'execution_time')
    return bar_plot(mean, errors=errors, figsize=(8, 6),
                    ylabel="Time (sec)", xlabel="",
                    legend_fontsize=22, legend_loc='best', **kwargs)


def mapred_failures(csv_path, awd):
    print ("Failures")
    df = read_mapred_csv(csv_path)
    df = filter_df(df)

    def failure_impact_prop(df, col):
        _t = lambda s: getattr(s, col).values[0]
        groupbycols = ['datasize', 'policy', 'nb_failures']
        dft = df.groupby(groupbycols)[col].mean().reset_index()
        dfte = df.groupby(groupbycols)[col].std().reset_index()
        print("-- failure impact (%s)" % col)
        for datasize, policy, dd, ee in _iterate_datasize_policy_2(dft, dfte):
            v0f = _t(dd[(dd.nb_failures == 0)])
            v1f = _t(dd[(dd.nb_failures == 1)])
            v0fe = _t(ee[(ee.nb_failures == 0)])
            v1fe = _t(ee[(ee.nb_failures == 1)])
            if 'size' in col:
                print(datasize, policy, (v1f-v0f) // 1024, 'KB')
            else:
                ratio = 100 * (v1f-v0f) / v0f
                # print(datasize, policy, round(ratio, 2), '%', 'slower')
                print("%3d %3s  (%6.2f->%6.2f)  (%5.2f->%5.2f)  %6.2f %% slower"
                      % (datasize, policy, v0f, v1f, v0fe, v1fe, ratio))

    def failure_impact(df):
        failure_impact_prop(df, 'execution_time')
        failure_impact_prop(df, 'map')
        failure_impact_prop(df, 'reduce')
        # failure_impact_prop(df, 'input_size')
        # failure_impact_prop(df, 'output_size')

    def add_failure_desc(x):
        stage, p, nb = x
        if stage == 'No-Failure':
            return stage
        return "%s %s\n%s%% %s\nprogress" % (nb,
        "Failures" if int(nb) > 1 else "Failure", p, stage)
        return "%s F\n%s:%s%%" % (nb, stage, p)

    df['failure'] = df[['failure_type', 'failure_stage', 'nb_failures']].apply(
                    add_failure_desc, axis=1)

    # print(df[(df['execution_time'] > 260) & (df.job_name == 'sorter')])
    if 'with-failure' in csv_path:
        df = df[df['execution_time'] < 250].copy()
    if 'with-kill' in csv_path:
        df = df[df['execution_time'] > 120].copy()

    for job_name in df.job_name.unique():
        print("%s%s%s" % (RED, job_name, NC))
        dfj = df[(df.job_name == job_name)]
        # failure_impact(dfj)

        jawd = get_rel_path(awd('%s/job/all-runs' % job_name))
        save_close(
            plot_execution_time_by_failure(dfj),
            jawd("job-execution-time"),
            fontsize=20, format="eps-pdf")


def mapred_mr_sp(csv_path, awd):
    print ("Overlapping vs. non-overlapping shuffle")
    df = read_mapred_csv(csv_path)
    df = filter_df(df)

    _f = lambda df, col, val: df[(getattr(df, col) == val)]
    # _t = lambda df, col: getattr(df, col).values[0]

    def shuffle_overlap_impact_prop(df, col):
        _t = lambda s: getattr(s, col).values[0]
        groupbycols = ['datasize', 'policy', 'overlapping_shuffle']
        dft = df.groupby(groupbycols)[col].mean().reset_index()
        dfte = df.groupby(groupbycols)[col].std().reset_index()
        print("-- The impact of non-overlapped shuffle (spark config): (%s)" % col)
        for datasize, policy, dd, ee in _iterate_datasize_policy_2(dft, dfte):
            vspark = _t(_f(dd, 'overlapping_shuffle', 'spark'))
            vhadoop = _t(_f(dd, 'overlapping_shuffle', 'hadoop'))
            vspark_e = _t(_f(ee, 'overlapping_shuffle', 'spark'))
            vhadoop_e = _t(_f(ee, 'overlapping_shuffle', 'hadoop'))
            ratio = 100 * (vspark-vhadoop) / vhadoop
            # print(datasize, policy, round(ratio, 2), '%', 'slower')
            # print("%d %s\t(%.2f->%.2f)\t%.2f %% slower" % (datasize, policy, vhadoop, vspark, ratio))
            print("%3d %3s  (%6.2f->%6.2f)  (%5.2f->%5.2f)  %6.2f %% slower"
                  % (datasize, policy, vhadoop, vspark, vhadoop_e, vspark_e, ratio))

    def shuffle_overlap_impact(df):
        shuffle_overlap_impact_prop(df, 'execution_time')
        shuffle_overlap_impact_prop(df, 'map')
        shuffle_overlap_impact_prop(df, 'reduce')

    for job_name in df.job_name.unique():
        print("%s%s%s" % (RED, job_name, NC))
        dff = df[(df.job_name == job_name)]
        shuffle_overlap_impact(dff)


def mapred_all_hardware(csv_path, awd):
    print ("All hardware")
    df = read_mapred_csv(csv_path)
    df = filter_df(df)

    _f = lambda df, col, val: df[(getattr(df, col) == val)]
    _t = lambda df, col: getattr(df, col).values[0]

    def network_impact_prop(df, col):
        _t = lambda s: getattr(s, col).values[0]
        groupbycols = ['datasize', 'policy', 'network_bw']
        dft = df.groupby(groupbycols)[col].mean().reset_index()
        dfte = df.groupby(groupbycols)[col].std().reset_index()
        print("-- network impact: using 1Gb network (%s)" % col)
        for datasize, policy, dd, ee in _iterate_datasize_policy_2(dft, dfte):
            v1Gb = _t(_f(dd, 'network_bw', '1Gb'))
            v10Gb = _t(_f(dd, 'network_bw', '10Gb'))
            v1Gbe = _t(_f(ee, 'network_bw', '1Gb'))
            v10Gbe = _t(_f(ee, 'network_bw', '10Gb'))
            if 'size' in col:
                print(datasize, policy, (v1Gb-v10Gb) // 1024, 'KB')
            else:
                ratio = 100 * (v1Gb-v10Gb) / v10Gb
                # print(datasize, policy, round(ratio, 2), '%', 'slower')
                # print("%d %s\t(%.2f->%.2f)\t%.2f %% slower" % (datasize, policy, v10Gb, v1Gb, ratio))
                print("%3d %3s  (%6.2f->%6.2f)  (%5.2f->%5.2f)  %6.2f %% slower"
                      % (datasize, policy, v10Gb, v1Gb, v10Gbe, v1Gbe, ratio))

    def network_impact(df):
        dft = df[(df.storage == 'HDD')]
        network_impact_prop(dft, 'execution_time')
        network_impact_prop(dft, 'map_mean')
        network_impact_prop(dft, 'reduce_mean')
        # network_impact_prop(dft, 'input_size')
        # network_impact_prop(dft, 'output_size')

    def ssd_impact_prop(df, col):
        _t = lambda s: getattr(s, col).values[0]
        dft = df.groupby(['datasize', 'storage', 'policy']
                        )[col].mean().reset_index()
        #print(dft)
        print("-- storage impact: using SSDs (%s)" % col)
        for datasize, policy, dd in _iterate_datasize_policy(dft):
            v1hdd = _t(dd[(dd.storage == 'HDD')])
            v1ssd = _t(dd[(dd.storage == 'SSD')])
            ratio = 100 * (v1hdd-v1ssd) / v1hdd
            ratio = v1hdd / v1ssd
            print(datasize, policy, round(ratio, 2), 'faster')

    def ssd_impact(df):
        dft = df[(df.network_bw == '10Gb') & (df.storage != 'MEM')]
        ssd_impact_prop(dft, 'execution_time')
        ssd_impact_prop(dft, 'map_mean')
        ssd_impact_prop(dft, 'reduce_mean')

    def memory_impact_prop(df, col):
        _t = lambda s: getattr(s, col).values[0]
        dft = df.groupby(['datasize', 'storage','policy']
                        )[col].mean().reset_index()
        #print(dft)
        print("-- storage impact: using MEM (%s)" % col)
        for datasize, policy, dd in _iterate_datasize_policy(dft):
            v1ssd = _t(dd[(dd.storage == 'SSD')])
            v1mem = _t(dd[(dd.storage == 'MEM')])
            ratio = 100 * (v1ssd-v1mem) / v1ssd
            # print(datasize, policy, round(ratio, 2), '%', 'faster')
            # ratio = v1ssd / v1mem
            print("%3d %3s  (%6.2f->%6.2f) %6.2f %% faster"
                  % (datasize, policy, v1ssd, v1mem, ratio))

    def memory_impact(df):
        dft = df[(df.network_bw == '10Gb') & (df.storage.isin(['SSD','MEM']))]
        memory_impact_prop(dft, 'execution_time')
        memory_impact_prop(dft, 'map_mean')
        memory_impact_prop(dft, 'reduce_mean')


    def as_table(df):
        print('Hardware configuration,datasize,EC,,,,,REP')
        print(',',',job,map (mean),reduce (mean),map (stdev),reduce (stdev)'*2)
        for storage in df.storage.unique():
            for network_bw in df.network_bw.unique():
                dft = df[(df.network_bw == network_bw)&(df.storage == storage)]
                if not len(dft): continue
                for datasize in sorted(dft.datasize.unique()):
                    print(storage, network_bw, ',', datasize, end='')
                    dd = dft[(dft.datasize == datasize)]
                    for policy in ('EC', 'REP'):
                        pp = dd[(dd.policy == policy)]
                        _t = lambda col: getattr(pp, col).values[0]
                        print(",%.2f,%.2f,%.2f,%.2f,%.2f" % (
                                _t('execution_time'),
                                _t('map_mean'), _t('reduce_mean'),
                                _t('map_stdev'), _t('reduce_stdev')), end='')
                    print()


    for job_name in df.job_name.unique():
        print("%s%s%s" % (RED, job_name, NC))
        dff = df[(df.job_name == job_name)]
        # network_impact(dff)
        # ssd_impact(dff)
        # memory_impact(dff)
        as_table(dff)



#
# Mapred analysis
#


def plot_tx_rx(df, **kwargs):

    mean, errors = pivot_df(df, 'datasize', 'policy', 'tx')

    if IS_STATS:
        print("-- Exchanged data (GB):")
        for datasize in sorted(df.datasize.unique()):
            n = mean.xs(datasize)
            ratio = 100*(n.EC-n.REP)/n.EC
            print("%3d [EC:%6.2f, REP:%6.2f] %6.2f %% ec has more overhead" % (
                  datasize, n.EC, n.REP, round(ratio, 2)))
        return None

    return bar_plot(mean, errors=errors,
                    # title="Exchanged data between worker",
                    ylabel="Exchanged data size (GB)", xlabel="Input data size (GB)")


def plot_load_distribution_box_core(df, **kwargs):
    pdf = df.pivot(index='node', columns='policy', values='dn_load')
    pdf /= ONE_MB
    if IS_STATS:
        # print(df)
        # print(df.describe())
        # print(pdf)
        # print(pdf.sum())
        # print(pdf.mean())
        print(pdf.std())
        # print(100*pdf.std()/pdf.mean())
        # print(pdf.median())
        return None

    ax = pdf.plot.box(legend=None, figsize=(2.6,3.6)) # , figsize=(2.6,3.6)
    ax.set_xlabel('')
    ax.set_ylabel('Data size (GB)')
    ax.set_title('')
    ax.grid(which='major', linestyle=':')
    return ax.get_figure()

def plot_load_distribution_box(df, **kwargs):
    policy, node, dn_load = [], [], []
    for index, row in df.iterrows():
        vals = [int(v) for v in row['load_distribution'].split('-')]
        policy.extend([row['policy']] * len(vals))
        dn_load.extend(vals)
        node.extend(range(len(vals)))
    dfm = pd.DataFrame.from_dict({'policy': policy, 'node':node, 'dn_load': dn_load})
    return plot_load_distribution_box_core(dfm, **kwargs)

def plot_load_distribution_box_written(df, **kwargs):
    policy, node, dn_load = [], [], []
    for index, row in df.iterrows():
        def parse_values(col):
            return sorted([int(v) for v in row[col].split('-')])
        vals1 = parse_values('lb_before_1')
        vals2 = parse_values('lb_after_1')
        vals = [x[0]-x[1] for x in zip(vals2, vals1)]
        policy.extend([row['policy']] * len(vals))
        dn_load.extend(vals)
        node.extend(range(len(vals)))
    dfm = pd.DataFrame.from_dict({'policy': policy, 'node':node, 'dn_load': dn_load})
    return plot_load_distribution_box_core(dfm, **kwargs)


def mapred_exchanged_data_per_job(df, output_dir):
    awd = get_rel_path(output_dir)

    # arawd = get_rel_path(awd('all-runs'))
    # save_close(
    #     plot_tx_rx(df),
    #     arawd("exchanged-data"),
    #     fontsize=24, format="pdf")

    # lb_before_1,lb_before_2,lb_after_1,lb_after_2

    # print(df)
    # return None

    for run_id in df.run_id.unique():
        # if run_id != 1: continue
        rawd = get_rel_path(awd('run-%d' % run_id))
        for datasize in sorted(df.datasize.unique()):
            if datasize != 40: continue
            dfd = df[(df.datasize == datasize) & (df.run_id == run_id)]
            # save_close(
            #     plot_load_distribution_box_written(dfd),
            #     rawd("byte_write-%dGB" % (datasize, )),
            #     fontsize=18, format="eps")
            # continue
            save_close(
                plot_load_distribution_box(dfd),
                rawd("dn-load-distribution-%dGB" % (datasize, )),
                fontsize=18, format="eps-pdf")


def mapred_exchanged_data(csv_path, awd):
    print ("Exchanged data")
    df = read_exchanged_data_csv(csv_path)
    df = filter_df(df)

    for job_name in df.job_name.unique():
        print('>>', job_name)
        df_job_name = df[(df.job_name == job_name)]
        mapred_exchanged_data_per_job(df_job_name, awd(job_name))


def plot_execution_time(df, **kwargs):

    mean, errors = pivot_df(df, 'datasize', 'policy', 'execution_time')

    if IS_STATS:
        print("-- Execution time: ")
        dfg = df.groupby(['datasize', 'policy'])['execution_time']
        print(df.groupby(['datasize', 'policy', 'run_id'])['execution_time'].mean())
        print(dfg.describe().transpose().round(2))
        print(mean.round(2))
        # print(errors.round(2))
        print(100*errors/mean)
        # return None
        # tm = mean.transpose()
        # base = tm.xs('RS(06, 3)')
        # base = tm.xs('01M')
        # # print(tm.round(2))
        # print((100 * (tm - base) / base).round(2))
        # return None
        for datasize in sorted(df.datasize.unique()):
            n = mean.xs(datasize)
            e = errors.xs(datasize)
            # print(n.xs('EC'))
            ratio = 100*(n.REP-n.EC)/n.REP
            print("%3d [EC:%6.2f, REP:%6.2f] (EC:%6.2f, REP:%6.2f) %6.2f %% ec is faster" % (
                  datasize, n.EC, n.REP, e.EC, e.REP, round(ratio, 2)))
        return None

    return bar_plot(mean, errors=errors,
                    # title="Job execution time",
                    ylabel="Time (sec)", xlabel="Input data size (GB)", **kwargs)


def plot_job_throughput(df, **kwargs):

    mean, errors = pivot_df(df, 'datasize', 'policy', 'throughput')
    return bar_plot(mean, errors=errors,
                    # title="Job throughput",
                    ylabel="Throughput (GB/sec)", xlabel="Input data size (GB)")


def plot_total_launched_maps(df, **kwargs):

    mean, errors = pivot_df(df, 'datasize', 'policy', 'total_launched_maps')
    return bar_plot(mean, errors=errors,
                    # title="Total launched maps",
                    ylabel="# maps", xlabel="Input data size (GB)")


def plot_data_local_maps(df, **kwargs):

    mean, errors = pivot_df(df, 'datasize', 'policy', 'data_local_maps')

    if IS_STATS:
        print("-- data local maps: ")
        for datasize in df.datasize.unique():
            if datasize < 90:
                continue
            n = mean.xs(datasize)
            mn, mx = min(n), max(n)
            ratio = 100*(mx-mn)/mx
            print(datasize, n, round(ratio, 2), '%')
        return None

    return bar_plot(mean, errors=errors,
                    title="Data-local maps",
                    ylabel="#maps", xlabel="Data size (GB)")


def plot_bar_props_by_policy(df, cols, legend_titles, **kwargs):

    grby = df.groupby(['policy', 'datasize'])[cols]
    mean_p = grby.mean()
    errors_p = grby.std()

    if IS_STATS:
        print(mean_p)
        return None

    fig, axes = plt.subplots(2,1, figsize=(6,10))
    for ax, tag in zip(axes, ('EC', 'REP')):
        mean, errors = mean_p.xs(tag), errors_p.xs(tag)
        mean.plot.bar(stacked=True, ax=ax, rot=0, sharex=True, yerr=errors)
        ax.set_title(tag)
        ax.set_ylabel("# maps")
        ax.set_xlabel("Data size (GB)")
        ax.legend(legend_titles, loc='upper left', fontsize='large')
        ax.grid(which='major', linestyle=':')
    return fig


def plot_local_maps(df, **kwargs):

    if IS_STATS:
        print("-- Tasks data locality: ")
        cols = ['data_local_maps', 'rack_local_maps', 'other_local_maps']
        grby = df.groupby(['datasize', 'run_id', 'policy'])[cols].mean()
        print(grby)
        return None

    return plot_bar_props_by_policy(df,
            ['data_local_maps', 'rack_local_maps', 'other_local_maps'],
            ["data-local maps", "rack-local maps", "other-local maps"],
            **kwargs)

def plot_maps_success_failure(df, **kwargs):
    return plot_bar_props_by_policy(df,
            ['num_succeeded_maps', 'num_killed_maps'],
            ["succeeded maps", "failed maps"],
            **kwargs)

def plot_execution_time_detail(df, **kwargs):

    color = kwargs.get('color', 'Pastel2')
    draw_errbars = kwargs.get('draw_errbars', False)

    if 'p_overlap' in df.columns:
        cols = ['p_map', 'p_overlap', 'p_red']
        legend_titles = ['Map phase', 'Overlapping', 'Reduce phase']
    else:
        cols = ['p_map', 'p_red']
        legend_titles = ['Map phase', 'Reduce phase']

    grby = df.groupby(['datasize', 'policy'])[cols]
    mean = grby.mean()
    errors = grby.std()

    if IS_STATS:
        print(mean)
        return None

    if draw_errbars:
        _, errors = pivot_df(df, 'datasize', 'policy', 'execution_time')
        err_param = {
        'yerr': errors,
        'error_kw': dict(ecolor='k', capsize=3, elinewidth=1, markeredgewidth=1)
        }
    else:
        err_param = {}

    ax = mean.plot.bar(cmap=color, stacked=True, rot=0, **err_param)

    # print(df[df.run_id==0].execution_time.unique())
    # ax.errorbar(range(8),df[df.run_id==0].execution_time.unique(),
    #             yerr=[[10,40,20,30,60,70,80,90],[41,100,60,80,140,170,180,190]], **err_param['error_kw'])

    ax.set_title('')
    ax.set_ylabel("Time (sec)")
    ax.set_xlabel("Data size (GB)")
    ax.legend(legend_titles, loc='upper left', fontsize=18)
    ax.grid(which='major', linestyle=':')
    # hatch_bars(df, ax)

    # coll = ax.collections[0]
    # coll.set_offset_position('data')
    #
    # points_xy = coll.get_offsets()
    # print (points_xy)
    # print(dir(ax.errorbar))

    # print(ax.get_lines())
    # print(ax.get_children())

    # for c in ax.get_children()[:2]:
    #     print(type(c))
    #
    #     print(c.get_transform())
    #
    #     # print(c.get_transform())
    #     trans = Affine2D().translate(1, 0)
    #     c.set_transform(trans)  # the points to pixels transform
    #
    #     continue
    #
    #     for getf in [p for p in dir(c) if p.startswith('get')]:
    #         if getf in ('get_cursor_data', 'get_datalim', 'get_window_extent'):
    #             continue
    #         try:
    #             print(getf, getattr(c, getf)())
    #         except Exception as e:
    #             print(getf)
    #     continue
    #     # print(dir(c))
    #
    #     return c.get_figure()
    #     # print(c.get_children())
    #     # print(c.get_cursor_data())
    #     print(c.get_offset_position())
    #     break
    #     # if c is LineCollection:
    #     #     print(c)
    #
    # return

    if True:

        if draw_errbars:
            # shift error bars
            xdata = [x[0]+x[1] for x in
                    zip([0.4, 0]*4, ax.lines[0].get_xdata())]
            for l in ax.lines:
                l.set_xdata(xdata)

        # ax.get_children()[1].set_xdata(xdata)

        cpbar = len(cols)  # colors per bar
        k = 6
        # Move back every second patch
        for i in range(cpbar):
            for j in range(0, k, 2):
                new_x = ax.patches[i*k+j].get_x() + 0.4
                ax.patches[i*k+j].set_x(new_x)

    # Update tick locations correspondingly
    import numpy as np
    minor_tick_locs = [x.get_x()+1/4 for x in ax.patches[:k]]
    major_tick_locs = np.array(minor_tick_locs).reshape(k//2, 2).mean(axis=1)
    ax.set_xticks(minor_tick_locs, minor=True)
    ax.set_xticks(major_tick_locs)

    # ax.errorbar(minor_tick_locs,minor_tick_locs, yerr=errors, ecolor='k', capsize=3,
    #               elinewidth=1, markeredgewidth=1)

    # Use indices from dataframe as tick labels
    minor_tick_labels = mean.index.levels[1][mean.index.labels[1]].values
    major_tick_labels = mean.index.levels[0].values
    ax.xaxis.set_ticklabels(minor_tick_labels, minor=True)
    ax.xaxis.set_ticklabels(major_tick_labels)
    # Remove ticks and organize tick labels to avoid overlap
    ax.tick_params(axis='x', which='both', bottom=False, labelsize='large')
    ax.tick_params(axis='x', which='minor', rotation=45)
    ax.tick_params(axis='x', which='major', pad=35, rotation=0)

    return ax.get_figure()

    return plot_bar_props_by_policy(df,
            ['p_init', 'p_map', 'p_mid', 'p_red', 'p_end'],
            ['p_init', 'p_map', 'p_mid', 'p_red', 'p_end'],
            **kwargs)



def plot_hibench_app(df):

    app_name = 'kmeans' if 'classification' in set(df.job_name.unique()) else 'pagerank'
    # df = df[df['job_name'] != 'classification']
    mean, errors = pivot_df(df, 'job_name', 'policy', 'execution_time')

    if IS_STATS:
        print("-- Execution time: ", app_name)
        print(mean)
        return None
        mean_sum = mean.sum()
        print(mean_sum)
        print(100 * (mean_sum - mean_sum.min()) / mean_sum.max(), '% slower/faster')
        # print(df.groupby('policy')['execution_time'].sum().reset_index().round(2))
        return None

    return bar_plot(mean, errors=errors, figsize=(10, 6),
                    # title="Job execution time",
                    legend_fontsize=22,
                    legend_loc='best' if app_name == 'pagerank' else 'upper middle',
                    ylabel="Time (sec)", xlabel="Job stages")


def plot_hibench_app_io(df):

    cols = ['hdfs_bytes_read', 'hdfs_bytes_written']
    df = df.copy()
    df['READ'] = df['hdfs_bytes_read']
    df['WRITTEN'] = df['hdfs_bytes_written']
    cols = ['READ', 'WRITTEN']
    grby = df.groupby('job_name')[cols]
    mean, errors = grby.mean(), grby.std()

    if IS_STATS:
        print("hibench app io")
        print(mean)
        print(mean.describe())
        print(mean.sum())
        return None

    return bar_plot(mean, errors=errors, figsize=(10, 6),
                    ylabel="Data size (GB)", xlabel="Job stages")


def plot_tasks_runtime_cdf_core(dfm, colname, **kwargs):

    datasize = kwargs.get("datasize")
    tasktype = kwargs.get("tasktype")
    title = kwargs.get("title", 'Data size [%dGB]' % datasize)

    pdf = dfm.pivot(columns='policy', values=colname)


    if IS_STATS:
        print('task type:', tasktype, datasize, 'GB', colname)
        mean_df, _ = groupby_mean_std(dfm, 'hostName', 'policy', colname)
        min_df, max_df = groupby_min_max(dfm, 'hostName', 'policy', colname)
        print(min_df.describe().round(2))
        print(max_df.describe().round(2))
        return None

    if IS_STATS:
        print('task type:', tasktype, datasize, 'GB', colname)
        print(pdf.describe().round(2))
        print(pdf.quantile(0.95))
        print(pdf.quantile(0.9))
        # print(pdf.quantile(0.95)/pdf.mean())
        # print(pdf.quantile(0.80)/pdf.mean())
        # print('variation:')
        # print(100*pdf.std()/pdf.mean())
        return None
        n = dfm.groupby('policy')[colname].mean()
        ratio = 100*(n.REP-n.EC)/n.REP
        print("%s: %3d [EC:%6.2f, REP:%6.2f] %6.2f %% ec is faster" % (
              tasktype[:3], datasize, n.EC, n.REP, round(ratio, 2)))
        print()
        return None

    if kwargs.get("plottype", "") == "box":
        ax = pdf.plot.box(legend=None)
        ax.set_ylabel('Task runtime (sec)')
        ax.set_ylim(40, ax.get_ylim()[1])
    else:
        ax = pdf.plot.hist(cumulative=True, density=1, bins=50, histtype='step')
        hatch_cdf_ec_rep(ax)
        ax.set_xlabel('Task runtime (sec)')
        # ax.set_title(title)
        ax.set_xlim(0, ax.get_xlim()[1]) # ???
        # ax.set_xlim(0, ax.set_xlim()[1])
    ax.grid(which='major', linestyle=':')
    return ax.get_figure()


def plot_jobs_start_finish_time(df):

    policy = 'EC'
    policy = 'REP'
    datasize = 10
    job_name = 'sorter'
    job_name = 'word count'
    run_id = 0

    df = df[(df.run_id == run_id)]
    df = df[(df.job_name == job_name)]
    df = df[(df.policy == policy)]
    df = df[(df.datasize == datasize)]

    minmin = min(df.launchedAt.min(), df.submittedAt.min(), df.finishedAt.min())

    df['launchedAt'] -= minmin
    df['submittedAt'] -= minmin
    df['finishedAt'] -= minmin

    pdf = df.filter(['submittedAt','launchedAt','finishedAt'], axis=1)

    print(policy, datasize, job_name)
    print(pdf)
    return None


def job_timeline_events(df):
    if len(df.run_id.unique()) > 1:
        print("job_timeline_events: Cannot aggreagate more than one run.")
        return
    header = ('datasize', 'policy',
               'submitted', 'launched', 'finished',
               'map_start', 'map_finish', 'red_start', 'red_finish',
               'map (%)', 'reduce (%)', 'overlap(%)',
               'map_phase', 'red_phase')
    print(("%10s |" * len(header)) % header)
    # for header in ('datasize', 'policy',
    #                'submitted', 'launched', 'finished',
    #                'map_start', 'map_finish', 'red_start', 'red_finish',
    #                'map (%)', 'reduce (%)', 'overlap(%)',
    #                'map_phase', 'red_phase'):
    #     print ("%10s |" % header, end='')
    # print()
    for datasize in sorted(df.datasize.unique()):
        # if datasize != 40: continue
        for policy in df.policy.unique():
            print ("%10s |%10s |" % (datasize, policy), end='')
            s = df[(df.datasize == datasize) & (df.policy == policy)]
            st = s.launchedAt.values[0]
            for prop in ('submittedAt', 'launchedAt', 'finishedAt',
                        'map_startTime', 'map_finishTime',
                        'reduce_startTime', 'reduce_finishTime'):
                tt = round(getattr(s, prop).values[0] - st, 2)
                print ("%10s |" % tt, end='')
            for prop in ('p_map_percent', 'p_red_percent', 'p_overlap_percent'):
                tt = round(getattr(s, prop).values[0], 2)
                print ("%10s |" % tt, end='')
            map_phase = round(getattr(s, 'map_finishTime').values[0], 2) - \
                        round(getattr(s, 'map_startTime').values[0], 2)
            red_phase = round(getattr(s, 'reduce_finishTime').values[0], 2) - \
                        round(getattr(s, 'reduce_startTime').values[0], 2)
            print ("%10.2f |" % map_phase, end='')
            print ("%10.2f |" % red_phase, end='')
            print()

    ss = df['launchedAt'] - df['submittedAt']
    print('Average wait time: %0.2f sec' % ss.mean())
    ss = df['map_startTime'] - df['launchedAt']
    print('Average map start time: %0.2f sec' % ss.mean())
    ss = df['reduce_startTime'] - df['map_finishTime']
    print('Average lag time between map and reduce: %0.2f sec' % ss.mean())
    ss = df['finishedAt'] - df['reduce_finishTime']
    print('Average time between red finish and job finish: %0.2f sec' % ss.mean())

def print_tasks_stats(df):

    T = ['total_launched_maps', 'total_launched_reduces']
    F = ['num_failed_maps', 'num_failed_reduces']
    K = ['num_killed_maps', 'num_killed_reduces']
    L = ['data_local_maps', 'rack_local_maps', 'other_local_maps']

    M = ['total_launched_maps', 'num_failed_maps', 'num_killed_maps']
    R = ['total_launched_reduces', 'num_failed_reduces', 'num_killed_reduces']

    cols = ['policy'] + L
    # print(df['total_launched_maps'] - df['num_failed_maps'] - df['num_killed_maps'])

    print(df.groupby(['datasize', 'run_id', 'policy'])[cols].mean())

    # print(    df['hdfs_bytes_read'] / ONE_GB)
    # print(    df['hdfs_bytes_written'] / ONE_GB)

    # for datasize in sorted(df.datasize.unique()):
    #     if datasize != 20: continue
    #     dfd = df[(df.datasize == datasize)]
    #     print(datasize, 'GB')
    #     print(dfd[cols])


def _jobs_core(df, output_dir):
    awd = get_rel_path(output_dir)

    def _job_level_plots():
        jawd = get_rel_path(awd('job'))

        save_close(
            plot_execution_time(df, legend_fontsize=22),
            jawd("execution-time"),
            fontsize=24, format="eps-pdf")

        # print(df)
        return

        # for f in ( 'png', ): # 'pdf', 'ps', 'eps', 'svg'
        #     save_close(
        #         plot_execution_time(df),
        #         jawd("job-execution-time-%s-d240" % f), format=f)

        # save_close(
        #     plot_execution_time(get_by_ec_cellsize(df), legend_title='Cell size'),
        #     jawd("exectime-ec-6-3-xM"))
        #
        # save_close(
        #     plot_execution_time(get_by_ec_schema(df), legend_fontsize=20),
        #     jawd("exectime-ec-x-y"),
        #     fontsize=24, format="eps-pdf")

        save_close(
            plot_execution_time_detail(df),
            jawd("execution-time-by-phase"),
            fontsize=24, format="png")
        # save_close(
        #     plot_job_throughput(df),
        #     jawd("job-throughput"))
        if IS_STATS:
            return
        if IS_MULTIJOBS:
            return
        return
        # save_close(
        #     plot_total_launched_maps(df),
        #     jawd("maps-total-launched"))
        # save_close(
        #     plot_data_local_maps(df),
        #     jawd("maps-data-local"))
        save_close(
            plot_local_maps(df),
            jawd("maps-locality"))
        # save_close(
        #     plot_maps_success_failure(df),
        #     jawd("maps-success-failure"))

    # if IS_STATS:
    #     job_timeline_events(df)
    #     return
    # return

    if len(df.job_name.unique()) > 1: # HiBench application
        save_close(
            plot_hibench_app(df),
            awd("exectime"),
            fontsize=24, format="eps-pdf")
        # save_close(
        #     plot_hibench_app_io(df),
        #     awd("io_size"),
        #     fontsize=20)

        # if 'classification' in set(df.job_name.unique()):

        return

    _job_level_plots()
    # print_tasks_stats(df)


def mapred_jobs_per_job(df, output_dir):
    awd = get_rel_path(output_dir)
    # if 'econome-20DN-10Gb-HDD/no-failure/vnew5-spark-1' in output_dir:
    #     df = df[~((df['execution_time'] > 120) & (df['datasize'] == 40))].copy()
    # if 'econome-20DN-10Gb-HDD/no-failure/all-10r/OL' in output_dir:
    #     df = df[~((df['execution_time'] > 160) & (df['datasize'] == 40))].copy()

    # df = df[df['execution_time'] < 250].copy()
    _jobs_core(df, awd('all-runs'))
    # if IS_STATS: return
    return
    if 'run_id' in list(df) and len(df.run_id.unique()) > 1:
        for run_id in sorted(df.run_id.unique()):
            # if run_id != 0: continue
            if IS_STATS:
                print('-'*40, run_id)
            _jobs_core(df[(df.run_id == run_id)], awd('run-%d' % run_id))
            # return None


def mapred_jobs(csv_path, awd):
    print ("Jobs' data")
    df = read_mapred_csv(csv_path)
    # print(df[df.policy.str.startswith(('ec-RS-6-3-8', 'ec-RS-6-3-3'))] [['policy', 'datasize']])
    # print(df.groupby('policy')['hdfs_bytes_read'].sum()// ONE_GB)
    # return

    df['datasize'] = df[['datasize', 'policy']].apply(reglage_datasize, axis=1)
    df = filter_df(df)
    # print(df['datasize'].unique())
    # print(df[df.policy.str.startswith(('ec-RS-6-3-3', ))] [['policy', 'datasize', 'execution_time']])
    # return

    if is_hibench_jobs(df):
        plot_hibench_jobs(df, awd, mapred_jobs_per_job)
        return

    if IS_MULTIJOBS:
        plot_jobs_start_finish_time(df)
        return
        df = df.groupby(['run_id', 'policy', 'job_name', 'datasize'])['execution_time'].mean().reset_index()
        df['throughput'] = df['datasize'] / df['execution_time']

    # in case of failure, modify dataset size
    # print(df.datasize.unique())
    # return
    mp = {4:5, 8:10, 9:10}
    df['datasize'] = df['datasize'].apply(lambda x: x if not x in mp else mp[x])

    for job_name in df.job_name.unique():
        print('>>', job_name)
        df_job_name = df[(df.job_name == job_name)]
        mapred_jobs_per_job(df_job_name, awd(job_name))




def plot_maxtime_time(df, **kwargs):
    mean, errors = pivot_df(df, 'datasize', 'policy', 'maxtime')
    return bar_plot(mean, errors=errors,
    # title="Map tasks max runtime",
                    ylabel="Time (sec)", xlabel="Input data size (GB)", **kwargs)

def max_map_jobs(csv_path, awd):
    def myround(x):
        if x < 20: return 15
        if x < 37: return 30
        return 45
    df = pd.read_csv(csv_path)
    df['datasize'] = df['hdfs_bytes_read'] // ONE_GB
    df['datasize'] = df['datasize'].apply(myround)
    save_close(
        plot_maxtime_time(df, legend_fontsize=16),
        awd("map-minmax-time"),
        fontsize=24, format="png", dpi=200)



def plot_job_timeline_per_node(df):

    duration = None
    for _, row in df.iterrows():
        if row['type'] == 'job':
            job_st = row['startTime']
            job_nd = row['finishTime']
            duration = job_nd - job_st
            break

    if duration is None:
        return None

    def normalize(t_st, t_nd):
        xmin = (t_st - job_st) / duration
        xmax = (t_nd - job_st) / duration
        return xmin, xmax

    tasks = [(row['type'], row['hostName'],
                row['startTime'], row['shuffleFinished'],
                row['sortFinished'], row['finishTime'])
                for _, row in df.iterrows()
                    if row['type'] != 'job']

    tasks.sort(key=lambda x: x[2])

    hostNames = sorted(set([t[1] for t in tasks]), key=lambda x:int(x.split('-')[1]))
    nb_datanodes = len(hostNames)

    if df_failure_info is not None:
        killed_hosts = set(list(df.killed_hosts.unique())[0].split(':'))
    else:
        killed_hosts = set()

    if IS_STATS:
        print(df.input_size.unique(), df.policy.unique(),
              "Number of Live nodes: %d" % nb_datanodes)
        killed_hosts = list(df.killed_hosts.unique())[0].split(':')
        print("Failed nodes:", killed_hosts)
        print(hostNames)
        print(nb_datanodes)
        return None
        # inputSplits = row['inputSplits'].split(':')
        for _, row in df.iterrows():
            if row['type'] == 'map':
                inputSplits = row['inputSplits'].split(':')
                if row['hostName'] in killed_hosts:
                    print(row['type'], row['hostName'], row['inputSplits'], row['hostName'] in set(inputSplits))
        return None

    fig, axes = plt.subplots(nb_datanodes, 1,
        figsize=(int(duration)//3, 2*nb_datanodes + max(len(tasks)//16, 2)))

    def draw_line(t_st, t_nd, y, lw, ls, c):
        xmin, xmax = normalize(t_st, t_nd)
        ax.axhline(y=y, linestyle=ls, linewidth=lw, color=c,
                    xmin=xmin, xmax=xmax)

    def draw_attempt_line(t_st, t_nd, y, c):
        draw_line(t_st, t_nd, y, 1, '-', c)
    def draw_task_line(t_st, t_nd, y, c):
        draw_line(t_st, t_nd, y, 0.4, ':', c)

    for hostname, ax in zip(hostNames, axes):
        index = 0
        for task in tasks:
            t_type, t_host, t_st, t_sh, t_sr, t_nd = task
            if t_host != hostname:
                continue
            index += 1
            if t_type == 'map':
                xmin, xmax = normalize(t_st, t_nd)
                ax.axhline(y=index, linestyle='-', linewidth=1, color='r',
                            xmin=xmin, xmax=xmax)
            elif t_type == 'reduce':
                xmin, xmax = normalize(t_st, t_sh)
                ax.axhline(y=index, linestyle='-', linewidth=1, color='b',
                            xmin=xmin, xmax=xmax)
                xmin, xmax = normalize(t_sh, t_sr)
                ax.axhline(y=index, linestyle='-', linewidth=1, color='m',
                            xmin=xmin, xmax=xmax)
                xmin, xmax = normalize(t_sr, t_nd)
                ax.axhline(y=index, linestyle='-', linewidth=1, color='g',
                            xmin=xmin, xmax=xmax)
        ax.set_ylabel('')
        ax.set_xlabel('')
        ax.set_title(hostname)
        ax.set_xlim(0, int(duration)+1)
        ax.set_ylim(0, index+1)
        if hostname in killed_hosts:
            ax.set_facecolor("lightslategray")
            # ax.set_facecolor("lightyellow")
            ftime = list(df.time_before_f.unique())[0] - job_st
            ax.axvline(x=ftime, linestyle=':', linewidth=2, color='k')
            ftime = list(df.time_after_f.unique())[0] - job_st
            ax.axvline(x=ftime, linestyle=':', linewidth=2, color='k')

        # ax.legend(title='')
        # ax.yaxis.set_major_locator(plt.NullLocator())
        ax.xaxis.set_major_locator(plt.MultipleLocator(base=10))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(base=5))
        ax.yaxis.set_major_locator(plt.MultipleLocator(base=2.0))
        ax.grid(which='major', linestyle=':')

    fig.suptitle("Number of Live nodes: %d" % nb_datanodes)
    return fig


def plot_job_timeline(df, **kwargs):

    figsize = kwargs.get('figsize', None)
    duration = kwargs.get('duration', None)

    for _, row in df.iterrows():
        if row['type'] == 'job':
            job_st = row['startTime']
            job_nd = row['finishTime']
            # print(duration)
            break

    if job_st is None and job_nd is None:
        return None

    if duration is None:
        duration = job_nd - job_st
    # import math
    # if math.isnan(duration):
    #     return None

    # print(df.nb_failures.unique())
    # print(df.failure_stage.unique())
    # print(df.killed_hosts.unique())
    # print(duration)
    # return None


    def normalize(t_st, t_nd):
        xmin = (t_st - job_st) / duration
        xmax = (t_nd - job_st) / duration
        return xmin, xmax

    if IS_STATS:
        yes, no = list(), list()
        for _, row in df.iterrows():
            if row['type'] == 'map':
                length = int(row['tFinishTime'] - row['tStartTime'])
                hosts = row['inputSplits'].split(':')
                stg1 = 'econome-18' in set(hosts[:6])
                stg2 = 'econome-15' in set(hosts[:6])
                stg3 = 'econome-12' in set(hosts[:6])
                stg4 = 'econome-7' in set(hosts[:6])
                stg = stg1 or stg2 #or  stg3
                if length > 95:
                    print(length, stg, sorted(hosts[:6]))
                if stg:
                    yes.append(length)
                else:
                    no.append(length)
        print('yes', len(yes), sum(yes)/len((yes)))
        print('no', len(no), sum(no)/len((no)))
        return None


        df_m = df[df['type'] == 'map']
        df_r = df[df['type'] == 'reduce']
        # df_m = df_r

        setup_t = df_m['startTime'] - df_m['tStartTime']
        clean_t = df_m['tFinishTime'] - df_m['finishTime']
        print(df.input_size.unique(), df.policy.unique())
        print("setup_t: %r" % sorted(setup_t.round().unique()))
        print("clean_t: %r" % sorted(clean_t.round().unique()))
        return None


        map_tasks_time = df_m['finishTime'] - df_m['startTime']
        red_tasks_time = df_r['finishTime'] - df_r['startTime']

        print(df.input_size.unique(), df.policy.unique())
        print("NB map tasks: %d" % len(map_tasks_time))
        print("NB red tasks: %d" % len(red_tasks_time))
        return None

        print(map_tasks_time.describe())
        print(red_tasks_time.describe())
        print()
        return None

    tasks = [(row['type'],
                row['startTime'], row['shuffleFinished'],
                row['sortFinished'], row['finishTime'],
                row['tStartTime'], row['tFinishTime'])
                for _, row in df.iterrows()
                    if row['type'] != 'job']

    tasks.sort(key=lambda x: x[1])

    if figsize is None:
        figsize = (int(duration)//3, max(len(tasks)//12, 2))
    fig, ax = plt.subplots(1,1, figsize=figsize)

    ax.axhline(y=0, linestyle='-', linewidth=2, color='k',
                xmin=0, xmax=duration)

    if df_failure_info is not None:
        # ftime = 20
        ftime = list(df.time_before_f.unique())[0] - job_st
        ax.axvline(x=ftime, linestyle=':', linewidth=2, color='k')
        ftime = list(df.time_after_f.unique())[0] - job_st
        ax.axvline(x=ftime, linestyle=':', linewidth=2, color='k')

        print(df.killed_hosts.unique())
        killed_hosts = list(df.killed_hosts.unique())[0].replace(':', '\n')
        text_msg = "%d failure(s) on\n%s" % (df.nb_failures.unique(), killed_hosts)
        ytext = int(0.9 * len(tasks))
        ax.annotate(text_msg, xy=(ftime, ytext), xytext=(ftime+5, ytext),
                    fontsize=18,
                    arrowprops=dict(facecolor='black', width=1),
                    )

    reduce_plot = False
    nb_map_tasks = len([1 for _, row in df.iterrows()
                    if row['type'] == 'map'])

    def draw_line(t_st, t_nd, y, lw, ls, c):
        xmin, xmax = normalize(t_st, t_nd)
        ax.axhline(y=y, linestyle=ls, linewidth=lw, color=c,
                    xmin=xmin, xmax=xmax)

    def draw_attempt_line(t_st, t_nd, y, c):
        draw_line(t_st, t_nd, y, 1, '-', c)
    def draw_task_line(t_st, t_nd, y, c):
        draw_line(t_st, t_nd, y, 0.4, ':', c)

    for index, task in enumerate(tasks):
        t_type, t_st, t_sh, t_sr, t_nd, tS, tF = task
        if t_type == 'map':
            draw_attempt_line(t_st, t_nd, index+1, 'r')
        elif t_type == 'reduce':
            y = index + 1 #  - nb_map_tasks
            draw_attempt_line(t_st, t_sh, y, 'b')
            draw_attempt_line(t_sh, t_sr, y, 'm')
            draw_attempt_line(t_sr, t_nd, y, 'g')
        else:
            print(t_type)
        # draw_task_line(tS, t_st, index+1, 'k')
        # draw_task_line(t_nd, tF, index+1, 'k')

    ax.set_ylabel('')
    ax.set_xlabel('Timestep')
    # ax.set_title('Timeline')
    # ncol=2, loc='upper center', bbox_to_anchor=(0.5, 1.20),
    ax.legend( # loc='lower right',
        fontsize='32', loc='lower right',
        # ncol=4, loc='upper center',  bbox_to_anchor=(0.5, 1.2),
        handles=[
        mpatches.Patch(color='r', label='Map tasks'),
        mpatches.Patch(color='b', label='Shuffle stage'),
        mpatches.Patch(color='m', label='Sort stage'),
        mpatches.Patch(color='g', label='Reduce stage')])
        # mpatches.Patch(color='b', label='Reduce tasks (shuffle)'),
        # mpatches.Patch(color='m', label='Reduce tasks (sort)'),
        # mpatches.Patch(color='g', label='Reduce tasks (reduce)')])

    ax.set_xlim(0, int(duration)+1)
    ax.xaxis.set_major_locator(plt.MultipleLocator(base=20))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(base=5))
    ax.grid(which='major', linestyle=':')
    return fig


def plot_blocks_read_per_node(df, **kwargs):

    policy, hostnames, nb_blocks, read_bytes, max_task_rt = [], [], [], [], []
    cnt = {'EC': dict(), 'REP': dict()}
    tasks_len_per_host = {'EC': dict(), 'REP': dict()}
    for index, row in df.iterrows():
        inputSplits = row['inputSplits'].split(':')
        cntp = cnt[row['policy']]
        if row['policy'] == 'REP':
            # it seems that the host is selected randomly, not the first one
            host = row['hostName'] if row['hostName'] in inputSplits else inputSplits[0]
            cntp[host] = cntp.get(host, 0) + 1
        else:
            for host in inputSplits[:6]:
                cntp[host] = cntp.get(host, 0) + 1
                # tasks_len_per_host[host] = cntp.get(host, list()) + [row['attempt_runtime']]
                tasks_len_per_host['EC'][host] = max(tasks_len_per_host['EC'].get(host, 0), row['attempt_runtime'])
    print(tasks_len_per_host['EC'])
    # print(sum(cnt['EC'].values()))
    # print(sum(cnt['REP'].values()))
    for p in ('EC', 'REP'): #
        policy.extend([p] * len(cnt[p]))
        nb_blocks.extend(cnt[p].values())
        hostnames.extend(cnt[p].keys())
        max_task_rt.extend(tasks_len_per_host['EC'][k] for k in cnt[p].keys())
        if p == 'EC':
            read_bytes.extend([256.0/6 * x for x in cnt[p].values()])
        else:
            read_bytes.extend([256.0 * x for x in cnt[p].values()])

    dfm = pd.DataFrame.from_dict({'policy': policy, 'node': hostnames,
                                  'nb_blocks': nb_blocks, 'read_bytes': read_bytes,
                                  'max_task_rt': max_task_rt})
    print(dfm)
    print(dfm.groupby('policy')['read_bytes'].describe().transpose())
    return None

    return plot_monitoring_prop_box(dfm, property='read_bytes')



# def plot_blocks_read_per_node(df, **kwargs):
#
#     policy, hostnames, nb_blocks, read_bytes = [], [], [], []
#     cnt = {p: dict() for p in df.policy.unique()}
#     for index, row in df.iterrows():
#         inputSplits = row['inputSplits'].split(':')
#         cntp = cnt[row['policy']]
#         if row['policy'] == 'REP':
#             # it seems that the host is selected randomely, not the first one
#             host = row['hostName'] if row['hostName'] in inputSplits else inputSplits[0]
#             cntp[host] = cntp.get(host, 0) + 1
#         else:
#             print(row['policy'] ,len(inputSplits))
#             print(inputSplits)
#             continue
#             nb_data_blocks = {10: 10, 9: 6, 5: 3, 3:3}[len(inputSplits)]
#             for host in inputSplits[:nb_data_blocks]:
#                 cntp[host] = cntp.get(host, 0) + 1
#     # print(cnt)
#     # for k,v in cnt.items():
#     #     print(k, sum(v.values()))
#     return None
#     # print(sum(cnt['EC'].values()))
#     # print(sum(cnt['REP'].values()))
#     for p in df.policy.unique():
#         policy.extend([p] * len(cnt[p]))
#         nb_blocks.extend(cnt[p].values())
#         hostnames.extend(cnt[p].keys())
#         if p == 'REP':
#             read_bytes.extend([256.0 * x for x in cnt[p].values()])
#         else:
#             d = {'RS(10, 4)': 10, 'RS(06, 3)': 6, 'RS(03, 2)': 3}[p]
#             read_bytes.extend([256.0/d * x for x in cnt[p].values()])
#
#     dfm = pd.DataFrame.from_dict({'policy': policy, 'node': hostnames,
#                                   'nb_blocks': nb_blocks, 'read_bytes': read_bytes})
#     # print(dfm)
#     # print(dfm.groupby('policy')['read_bytes'].describe())
#
#     return plot_monitoring_prop_box(dfm, property='read_bytes')



def _analyse_tasks(df, **kwargs):
    # sum_df, _ = groupby_mean_std(df, 'hostName', 'policy', 'hdfsBytesWritten')

    # df = df.copy()
    dft = df.copy()
    dft['job_startTime'] = dft['startTime']
    dft = dft.groupby(['run_id', 'policy', 'job_name', 'input_size'])['job_startTime'].min().reset_index()

    df = pd.merge(df, dft, on=['run_id', 'job_name', 'policy', 'input_size'], how='left')

    # print(df[pd.isnull(df.job_startTime)].type.unique())
    # print(sorted(df['job_startTime'].unique()))
    # print(sorted(df[df.type == 'job']['startTime'].unique()))
    # return

    dfm = df[df.type == 'map'].copy()
    dfr = df[df.type == 'reduce']

    # sum_df = df[df.type == 'map'].groupby('policy')['hdfsBytesRead']
    # print(sum_df.describe())
    # sum_df = df[df.type == 'reduce'].groupby('policy')['hdfsBytesWritten']
    # print(sum_df.describe())

    # sum_df = df[df.type == 'map'].groupby('policy')['cpuMilliSeconds']
    # print(sum_df.describe())
    # sum_df = df[df.type == 'reduce'].groupby('policy')['cpuMilliSeconds']
    # print(sum_df.describe())

    # print(df['cpuMilliSeconds'].corr(df['runtime']))
    # print(dfm['cpuMilliSeconds'].corr(dfm['runtime']))
    # print(dfr['cpuMilliSeconds'].corr(dfr['runtime']))

    def mark_killed_tasks(x):
        task_type, host_name, killed_hosts = x
        killed_hosts = set(killed_hosts.split(':'))
        if host_name in killed_hosts:
            return "Killed" # killed / should not appear !!
        return task_type

    def compute_task_status(x):
        policy, host_name, input_splits, killed_hosts = x
        input_splits = input_splits.split(':')[:6]
        input_splits_set = set(input_splits)
        killed_hosts = set(killed_hosts.split(':'))
        # if host_name in killed_hosts:
        #     return "Killed" # killed / should not appear !!
        if policy.lower().startswith('rep'):
            if host_name in input_splits_set:
                return "Local"
            if input_splits[0] in killed_hosts:
                return "DegRead"
            else:
                return "Remote"
        else:
            if killed_hosts & input_splits_set:
                return "DegRead"
            elif host_name in input_splits_set:
                return "Local"
            else:
                return "Remote"
        return 0

    # dfm = dfr
    dfm['task_type'] = (dfm['startTime'] - dfm['job_startTime']).apply(lambda x: "Initial" if x<20 else "Relaunched")
    dfm['task_type'] = dfm[['task_type', 'hostName', 'killed_hosts']].apply(mark_killed_tasks, axis=1)
    dfm['task_read'] = dfm[['policy', 'hostName', 'inputSplits', 'killed_hosts']].apply(compute_task_status, axis=1)
    dfm['detection_time'] = dfm['startTime'] - dfm['time_before_f'] #  'time_before_f', 'time_after_f'

    dfrl = dfm[dfm['task_type'] == "Relaunched"]
    dfg = dfrl.groupby(['policy', 'task_read'])['attempt_runtime']
    print("Relaunched Tasks")
    print(dfg.describe().round(2).transpose())
    # return


    for run_id in sorted(dfm.run_id.unique()):
        # break
        # if run_id > 2: continue
        print('run_id:', run_id)
        dfr = dfm[(dfm.run_id == run_id)]

        dfg = dfr.groupby(['policy', 'task_type'])['attempt_runtime']
        print("Tasks classification")
        print(dfg.describe().round(2).transpose())

        dfg = dfr.groupby(['policy', 'task_type', 'task_read'])['attempt_runtime']
        print("Tasks classification (detailed)")
        print(dfg.describe().round(2).transpose())

        # detection time
        # dfr = dfr[dfr.task_type == "Relaunched"]
        # dfg = dfr.groupby('policy')['detection_time']
        # # print(dfg.describe().transpose())
        # print(dfg.mean())

    # dfg = dfm.groupby(['policy', 'task_type', 'task_read'])['attempt_runtime']
    # print("Tasks classification (aggreagated runs)")
    # print(dfg.describe().round(2).transpose())


def tasks_as_table(df):
    colname = 'attempt_runtime' # 'attempt_runtime', 'task_runtime', 'red_func_runtime'

    dfg = df.groupby(['input_size', 'type', 'policy'])[colname]
    max_df = dfg.max().unstack().round(1)
    min_df = dfg.min().unstack().round(1)
    mean_df = dfg.mean().unstack().round(1)
    std_df = dfg.std().unstack().round(1)
    var_df = (100 * std_df / mean_df).round(1)

    print(',,,min,average,max,variation')
    for job_name in df.job_name.unique():
        for input_size in sorted(df.input_size.unique()):
            if input_size != 40: continue
            for policy in df.policy.unique():
                for tasktype in ('map', 'reduce',):
                    print('%s,%s,%s,%s' % (job_name, policy, tasktype,
                        ','.join([str(dff.xs(input_size).xs(tasktype).xs(policy))
                            for dff in (min_df, mean_df, max_df, var_df)])))


def mapred_attempts_per_job(df, output_dir):

    def analyse_tasks(df, awd):
        tawd = get_rel_path(awd('blocks-per-node'))
        for input_size in sorted(df.input_size.unique()):
            # if input_size != 40: continue
            pp = df[(df.input_size == input_size)]
            save_close(
                _analyse_tasks(pp),
                tawd("%dGB" % (input_size, )))

    def blocks_read_per_node(df, awd):
        tawd = get_rel_path(awd('blocks-per-node'))
        df = df[df.type == 'map']
        # df = df[df.policy == 'RS(10, 4)']
        for input_size in sorted(df.input_size.unique()):
            if input_size != 40: continue
            pp = df[(df.input_size == input_size)]
            save_close(
                plot_blocks_read_per_node(pp),
                tawd("%dGB" % (input_size, )))

    def tasks_runtime_cdf(df, awd):
        colname = 'attempt_runtime' # 'attempt_runtime', 'task_runtime', 'red_func_runtime', 'hdfsBytesRead'
        tawd = get_rel_path(awd('attempts-runtime-cdf'))
        for input_size in sorted(df.input_size.unique()):
            if input_size != 40: continue
            for tasktype in ('map', ): # 'map' 'reduce',
                pp = df[(df.input_size == input_size) &
                        (df.type == tasktype)]
                if not len(pp): continue
                # pp = pp[(pp.hostName == 'econome-15')]
                print('>>>>>>>>>>>>')
                # save_close(
                #     plot_tasks_runtime_cdf_core(pp, colname,
                #         datasize=input_size, tasktype=tasktype),
                #     tawd("%s-%d" % (tasktype, input_size)))

                save_close(
                    plot_tasks_runtime_cdf_core(pp, colname, plottype="box",
                        datasize=input_size, tasktype=tasktype),
                    tawd("%s-%d-box" % (tasktype, input_size)))

                # save_close(
                #     plot_tasks_runtime_cdf_core(get_by_ec_cellsize(pp), colname,
                #         datasize=input_size, tasktype=tasktype),
                #     tawd("%s-%d-ec-6-3-xM" % (tasktype, input_size)))
                # save_close(
                #     plot_tasks_runtime_cdf_core(get_by_ec_schema(pp), colname,
                #         datasize=input_size, tasktype=tasktype),
                #     tawd("%s-%d-ec-x-y" % (tasktype, input_size)))


    def job_timeline(df, awd):
        # if IS_STATS: return
        jawd = get_rel_path(awd('attempts-timeline'))
        for input_size in df.input_size.unique():
            if input_size != 40: continue
            for policy in df.policy.unique():
                if policy == 'REP': continue
                pp = df[(df.input_size == input_size) &
                        (df.policy == policy)]
                save_close(
                    plot_job_timeline(pp, duration=120, figsize=(16, 16)), # duration=120, figsize=(12, 10)
                    jawd("%s-%d-new" % (policy, input_size)),
                    fontsize=52, format="png")
                # save_close(
                #     plot_job_timeline_per_node(pp),
                #     jawd("%s-%d-per-node" % (policy, input_size)))

    def _job_timeline_test_skew(df):
        from itertools import groupby
        tasks = [(row['type'], row['hostName'],
                     - row['startTime'] + row['finishTime'])
                    for _, row in df.iterrows()
                        if row['type'] == 'map']
                            # if row['type'] == 'reduce']
                            # if row['type'] != 'job']
        tasks.sort(key=lambda x: x[2], reverse=True)
        # for task in tasks[:20]:
        #     print(task)
        top20p = int(0.2 * len(tasks))
        topk = tasks[:top20p]
        topk.sort(key=lambda x: x[1])
        lengths = [(key, len(list(group)))
            for key, group in groupby(topk, lambda x: x[1])]
        # for key, group in groupby(topk, lambda x: x[1]):
        #     print(key, len(list(group)))
        lengths.sort(key=lambda x: x[1], reverse=True)
        for length in lengths:
            if length[1]>1:
                print(length)
        return
        tasks.sort(key=lambda x: x[1])
        for key, group in groupby(tasks, lambda x: x[1]):
            sum_times, count = 0, 0
            for row in group:
                sum_times += row[2]
                count += 1
            avg = 1.0 * sum_times / count
            print(key, avg)

    def job_timeline_test_skew(df, awd):
        # print(len(df.hostName.unique()))
        # print(df.hostName.unique())
        # return
        dt = df.copy()
        dt['datasize'] = dt['input_size']
        for datasize, policy, dd in _iterate_datasize_policy(dt):
            print(datasize, policy)
            _job_timeline_test_skew(dd)

    awd = get_rel_path(output_dir)
    # tasks_runtime_cdf(df, get_rel_path(awd('all-runs')))
    # blocks_read_per_node(df, get_rel_path(awd('all-runs')))
    # analyse_tasks(df, get_rel_path(awd('all-runs')))
    # return

    for run_id in df.run_id.unique():
        if run_id != 4: continue
        if IS_STATS:
            print('-'*40, run_id)
        dfr = df[(df.run_id == run_id)]
        awdr = get_rel_path(awd('run-%d' % run_id))
        # blocks_read_per_node(dfr, awdr)
        # tasks_runtime_cdf(dfr, awdr)
        job_timeline(dfr, awdr)
        # job_timeline_test_skew(dfr, awdr)


def mapred_attempts(csv_path, awd):
    print ("Jobs' attempts")
    df = read_attempts_csv(csv_path)
    df = filter_df(df)

    if is_hibench_jobs(df):
        plot_hibench_jobs(df, awd, mapred_attempts_per_job)
        return

    mp = {9:10, 7:8}
    df['input_size'] = df['input_size'].apply(lambda x: x if not x in mp else mp[x])

    # df['input_size'] = df[['input_size', 'policy']].apply(reglage_datasize, axis=1)
    print(df['input_size'].unique())
    # print(df[df.policy.str.startswith(('ec-RS-6-3-8', 'ec-RS-6-3-3'))] [['policy', 'input_size']])

    if df_failure_info is not None:
        df = pd.merge(df, df_failure_info, on=['run_id', 'job_name', 'policy', 'input_size'], how='left')

    # tasks_as_table(df)
    # return

    for job_name in df.job_name.unique():
        print('>>', job_name)
        df_job_name = df[(df.job_name == job_name)]
        mapred_attempts_per_job(df_job_name, awd(job_name))


def plot_monitoring_prop_box(df, **kwargs):

    # df = extract_map_phase(df).copy()
    # print(list(df['read_bytes'].unique()))
    sum_df, _ = groupby_sum_std(df, 'node', 'policy', kwargs['property'])
    sum_df /= 1024
    if 'cpu' in kwargs['property']:
        # sum_df, _ = groupby_q50_q75(df, 'node', 'policy', kwargs['property'])
        # sum_df, _ = groupby_min_max(df, 'node', 'policy', kwargs['property'])
        sum_df, _ = groupby_mean_std(df, 'node', 'policy', kwargs['property'])

    if IS_STATS:
        print("%d GB [%s]" % (df.datasize.unique()[0], kwargs['property']))
        # print(sum_df.xs('econome-21').xs('ec'))
        # print(sum_df.xs('econome-15').xs('ec'))
        # print(sum_df.xs('econome-7').xs('ec'))
        # print(sum_df.xs('econome-8').xs('ec'))
        print(sum_df)
        print(" - Total (sum):\n%r" % sum_df.sum())
        print(" - Per node:\n%r" % sum_df.describe())
        print(" - Std dev:\n%r" % sum_df.std())

        # print("Data read per node")
        # print("```")
        # print(" - Variation:\n%r" % (100*sum_df.std()/sum_df.mean()))
        # print("```")
        print()
        return None
    print(" - Per node:\n%r" % sum_df.describe())
    ax = sum_df.plot.box(legend=None, figsize=(2.6,3.6)) #
    ax.set_ylabel('Data size (GB)')
    ax.set_ylim(0, ax.set_ylim()[1])
    # ax.set_xticklabels(['EC', 'REP'])
    ax.grid(which='major', linestyle=':')
    return ax.get_figure()


def plot_dn_monitoring_policy(df, **kwargs):

    print(df.run_id.unique(), df.policy.unique(), df.datasize.unique())

    df = extract_map_phase(df).copy()
    dfj = df_jobs[(df_jobs.policy == df.policy.unique()[0]) &
                  (df_jobs.datasize == df.datasize.unique()[0]) &
                  (df_jobs.job_name == df.job_name.unique()[0]) &
                  (df_jobs.run_id == df.run_id.unique()[0])]

    dt = dfj.submittedAt.values[0] - df.time.min()
    job_start_t = dt + dfj.launchedAt.values[0] - dfj.submittedAt.values[0]
    map_finish_t = dt + dfj.reduce_startTime.values[0] - dfj.submittedAt.values[0]
    job_finish_t = dt + dfj.finishedAt.values[0] - dfj.submittedAt.values[0]
    # print(job_start_t)
    # print(map_finish_t)
    # print(job_finish_t)
    # print(df.time.min())
    # print(dfj.submittedAt.values[0])
    # return

    fig, axes = plt.subplots(4,1, figsize=(5+df.timestep.max()//8, 4*4))

    def _plot(cols, index, ylabel, xlabel):
        if IS_STATS:
            print("--++++----")
            # ff = df[df.read_bytes > 0].groupby(['timestep'])[cols].mean().describe()
            ff = df.groupby(['timestep'])[cols].mean().describe()
            ff = df.groupby(['node'])[cols].mean().describe()
            print(ff)
            return
        ax = df.groupby(['timestep'])[cols].mean().plot(ax=axes[index])
        # ax.set_title('')
        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)
        ax.legend(loc='upper left', fontsize='xx-large').set_title('')
        ax.xaxis.set_major_locator(plt.MultipleLocator(base=10.0))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(base=5.0))
        ax.grid(which='major', linestyle=':')

        for _t in (job_start_t,map_finish_t,job_finish_t):
            ax.axvline(x=_t, linestyle=':', linewidth=1, color='k')

        # print(df.killed_hosts.unique())
        # killed_hosts = list(df.killed_hosts.unique())[0].replace(':', '\n')
        # text_msg = "%d failure(s) on\n%s" % (df.nb_failures.unique(), killed_hosts)
        # ytext = int(0.9 * len(tasks))
        # ax.annotate(text_msg, xy=(ftime, ytext), xytext=(ftime+5, ytext),
        #             fontsize=18,
        #             arrowprops=dict(facecolor='black', width=1),
        #             )

    _plot(['cpu_SUM','cpu_user','cpu_system','cpu_iowait'], 0, 'CPU %', '')
    # _plot(['mem_used','mem_cached'], 1, 'MEM (GB)', '') # 'mem_total',
    # _plot(['read_bytes', 'write_bytes'], 2, 'DISK (MB)', '')
    # _plot(['bytes_sent', 'bytes_recv'], 3, 'NETWORK (MB)', 'Timestep')

    if IS_STATS:
        return
    return fig


def plot_dn_monitoring(df, **kwargs):

    datasize = kwargs['datasize']
    node = kwargs.get("node", None)

    df = df[(df.datasize == datasize)]
    if node is not None:
        df = df[(df.node == node)]

    props_original = {
              'cpu_utilization' : ('CPU utilization %', ),
              'read_count' : ('# disk read op', ),
              'read_bytes' : ('disk read (MB)', ),
              'write_count' : ('# disk write op', ),
              'write_bytes' : ('disk write (MB)', ),
              # 'disk_io' : ('disk IO (MB)', ),
              'bytes_sent' : ('network sent (MB)', ),
              'bytes_recv' : ('network recv (MB)', )}

    props_new = {
              'cpu_user' : ('cpu_user %', ),
              'cpu_system' : ('cpu_system %', ),
              'cpu_iowait' : ('cpu_iowait %', ),
              'cpu_idle' : ('cpu_idle %', ),
              # 'mem_total' : ('mem_total (MB)', ),
              # 'mem_available' : ('mem_available (MB)', ),
              # 'mem_used' : ('mem_used (MB)', ),
              # 'mem_free' : ('mem_free (MB)', ),
              # 'mem_buffers' : ('mem_buffers (MB)', ),
              # 'mem_cached' : ('mem_cached (MB)', )
              }

    if 'cpu_utilization' in list(df):
        props = props_original
    else:
        props = props_original
        props.update(props_new)
        del props['cpu_utilization']

    if IS_STATS:
        print("-- Monitoring metrics for %dGB input" % (datasize, ))
        if node is None:
            for n in sorted(df.node.unique()):
                # mean, errors = pivot_df(df, 'timestep', 'policy', 'read_bytes')
                # print(mean)
                break
                df_ec = df[(df.node == n) & (df.policy == 'ec')]
                total_read_disk = df_ec['read_bytes'].sum()
                total_send_network = df_ec['bytes_sent'].sum()
                print("%s:\t%d %d" % (n, total_read_disk, total_send_network))
            # mean, errors = pivot_df(df, 'node', 'policy', 'read_bytes')

            rip = lambda x: x().reset_index().pivot(
                            index='node', columns='policy', values='read_bytes')
            dfg = df.groupby(['node', 'policy'])['read_bytes']
            sum_df = rip(dfg.sum)

            # sum_df = df.groupby(['node', 'policy'])['read_bytes', 'bytes_sent'].sum()
            # print(sum_df)
            print(sum_df.describe())
        return None

    fig, axes = plt.subplots(len(props),1, figsize=(24, len(props) * 5))
    for i, prop in enumerate(props.keys()):
        mean, errors = pivot_df(df, 'timestep', 'policy', prop)

        if IS_STATS:
            if prop in ('read_bytes', 'bytes_sent'):
                ylabel = props[prop][0]
                print(">>> %s" % (ylabel, ))
                print(mean.describe())
            continue

        ax = mean.plot(ax=axes[i], rot=0) # yerr=errors , sharex=(i==1),

        ylabel = props[prop][0]
        ax.set_title('')
        ax.set_ylabel(ylabel)
        ax.set_xlabel("Timestep" if i==(len(props.keys())-1) else '')
        ax.legend(loc='upper left', fontsize='xx-large').set_title('')
        ax.xaxis.set_major_locator(plt.MultipleLocator(base=10.0))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(base=5.0))
        ax.grid(which='major', linestyle=':')

    # fig.subplots_adjust(hspace=1.3, wspace=1.3)
    # fig.suptitle('Average DataNode metrics for %dGB input]' % (datasize, ), fontsize=20)

    if IS_STATS:
        return None

    return fig


def plot_heatmap_timeline(df, policy, heat_column, **kwargs):

    datasize = kwargs.get('datasize', 0)
    is_sum_all = kwargs.get('is_sum_all', False)
    color = kwargs.get('color', 'Reds')
    # color = kwargs.get('color', 'RdYlGn_r')

    df = df[(df.policy == policy)].copy()
    if is_sum_all:
        cols = ['node', 'datasize']
        ylabel, xlabel = 'Datasize (GB)', ''
    else:
        df = df[(df.datasize == datasize)]
        cols = ['timestep', 'node']
        ylabel, xlabel = '', 'Timestep'

    # df = df[(df.timestep < (df.timestep.unique().size-1))]
    # df['timestep'] = df['timestep'].apply(lambda x: x-2)
    fig_xlength = int(df.timestep.unique().size // 6) + 3

    df['node'] = df['node'].apply(lambda x: "node %02s" % x.split('-')[1])

    if IS_STATS:
        print(policy)
        df = df[df[heat_column] > 10].groupby('timestep')[heat_column].sum()
        print(df.describe())
        return None

    df = df.groupby(cols)[heat_column].sum().unstack(-1)

    ax = sns.heatmap(df.T, cmap=color, linewidths=0.5, square=True,
        annot=is_sum_all, fmt=".2f", vmin=0, vmax=125) # , xticklabels=10, yticklabels=False
    ax.set_title('')
    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    for item in ax.get_yticklabels():
        item.set_rotation(0)
    for item in ax.get_xticklabels():
        item.set_rotation(0)
    ax.get_figure().set_size_inches(max(10, fig_xlength), 5)
    return ax.get_figure()


def _monitoring_core(df, awd):

    def dn_monitoring_box():
        dawd = get_rel_path(awd('dn-monitoring-box'))
        for datasize in sorted(df.datasize.unique()):
            if datasize != 40: continue
            dfd = df[(df.datasize == datasize)]
            # for prop in ('read_bytes', 'write_bytes', 'bytes_sent', 'bytes_recv'):
            # for prop in ('read_count', 'write_count', 'cpu_SUM', 'cpu_iowait'):
            for prop in ('bytes_sent', 'read_bytes', 'write_bytes',): # 'read_bytes', 'write_bytes',
                if prop != 'read_bytes': continue
                save_close(
                    plot_monitoring_prop_box(dfd, property=prop),
                    dawd("%s-%dGB" % (prop.replace('_','-'), datasize, )),
                    fontsize=18, format="eps-pdf")

    def dn_monitoring():
        mawd = get_rel_path(awd('dn-monitoring'))
        for datasize in sorted(df.datasize.unique()):
            save_close(
                plot_dn_monitoring(df, datasize=datasize),
                mawd("avg-dn-monitoring-%dGB" % (datasize, )))
            continue
            # if datasize > 50: continue
            nwd = get_rel_path(mawd("avg-dn-monitoring-%dGB" % (datasize, )))
            for node in sorted(df.node.unique()):
                save_close(
                    plot_dn_monitoring(df, datasize=datasize, node=node),
                    nwd("node-%s" % (node, )))

    def dn_monitoring_policy():
        if 'cpu_utilization' in list(df):
            return
        mawd = get_rel_path(awd('dn-monitoring'))
        for policy in sorted(df.policy.unique()):
            pmawd = get_rel_path(mawd(policy))
            dfp = df[(df.policy == policy)]
            for datasize in sorted(df.datasize.unique()):
                if datasize != 40: continue
                dfd = dfp[(dfp.datasize == datasize)]
                save_close(
                    plot_dn_monitoring_policy(dfd),
                    pmawd("avg-dn-monitoring-%dGB" % datasize))
                continue
                nwd = get_rel_path(pmawd("avg-dn-monitoring-%dGB" % datasize))
                for node in sorted(dfd.node.unique())[:5]:
                    save_close(
                        plot_dn_monitoring_policy(dfd[(dfd.node == node)]),
                        nwd("node-%s" % (node, )))

    def dn_heatmap_timeline():
        for prop in ('read_bytes', ): # 'write_bytes'
            cwd = get_rel_path(awd("%s-heatmap" % prop))
            for policy in df.policy.unique():
                # save_close(
                #     plot_heatmap_timeline(df, policy, prop, is_sum_all=True),
                #     cwd("%s" % (policy, )))
                # continue
                for datasize in sorted(df.datasize.unique()):
                    if datasize != 40: continue
                    save_close(
                        plot_heatmap_timeline(df, policy, prop, datasize=datasize),
                        cwd("%s-%dGB" % (policy, datasize)))

    df = extract_map_phase(df)
    # df = extract_reduce_phase(df)
    # df = extract_reduce_stage(df)

    # df = extract_kmeans_job(df, 'c')
    # df_org = df.copy()
    # for itr in range(10):
    #     itr_str = "I-%02d" % (itr+1)
    #     df = extract_kmeans_job(df_org, itr_str)
    #     dn_monitoring_box()

    # df = df[df.timestep > 190].copy()

    dn_monitoring_box()
    # dn_monitoring()
    # dn_monitoring_policy()
    # dn_heatmap_timeline()


def datanodes_monitoring_per_job(df, output_dir):
    awd = get_rel_path(output_dir)
    for run_id in df.run_id.unique():
        if run_id != 4: continue
        print('run_id', run_id)
        _monitoring_core(df[(df.run_id == run_id)], get_rel_path(awd('run-%d' % run_id)))


def datanodes_monitoring(csv_path, awd):
    print ("DataNodes monitoring")
    df = read_monitoring_csv(csv_path)
    df = filter_df(df)

    # print(df.to_csv(index=0))
    # return

    df['disk_io'] = df['read_bytes'] + df['write_bytes']
    # df['mem_total'] //= 2

    for job_name in df.job_name.unique():
        # if job_name != 'kmeans': continue
        print('>>', job_name)
        df_job_name = df[(df.job_name == job_name)]
        datanodes_monitoring_per_job(df_job_name, awd(job_name))


def _blocks_distribution_core(df, awd):

    df = df[df.filename.str.startswith('/data/input/part-m')].copy()
    # df = df[df.filename.str.startswith('/data/output/part-r')].copy()

    df = df[df.type == 'data'].copy()
    # df = df[df.type == 'parity'].copy()

    # 'ec-RS-x-y-1024k'
    # df['nb_data'] = df['policy'].apply(lambda x: int(x.split('-')[2]))
    df['nb_data'] = 6
    df['size'] = df['len'] / df['nb_data']

    # def parse_filepath(hdfspath):
    #     size, nb_files, filename_host = hdfspath[6:].split('/')
    #     return 10*(int(nb_files)+1), int(size)
    #
    # df['policy'] = 'ec'
    # df[['nb_files', 'file_size_mb']] = df['filename'].apply(
    #     lambda hdfspath: pd.Series(parse_filepath(hdfspath)))

    df['nb_blocks'] = 1
    df1 = df.groupby(['policy', 'host', 'datasize'])['nb_blocks'].count().reset_index()
    df1['read_bytes'] = 256 * df1['nb_blocks']
    df1['node'] = df1['host']

    df2 = df.groupby(['policy', 'host', 'datasize'])['size'].sum().reset_index()
    df2['read_bytes'] = df2['size'] // ONE_MB
    df2['node'] = df2['host']

    df = df2
    for datasize in sorted(df.datasize.unique()):
        if datasize != 40: continue
        print('datasize:', datasize)
        dfd = df[(df.datasize == datasize)]
        save_close(
            plot_monitoring_prop_box(dfd, property='read_bytes'),
            awd("blocks/read-map-%dGB" % (datasize, )))


def blocks_distribution_per_job(df, output_dir):
    awd = get_rel_path(output_dir)
    for run_id in sorted(df.run_id.unique()):
        # if run_id != 2: continue
        print('run id:', run_id)
        _blocks_distribution_core(df[(df.run_id == run_id)], get_rel_path(awd('run-%d' % run_id)))

def blocks_distribution(csv_path, awd):
    print("EC blocks distribution")
    df = read_blocks_csv(csv_path)
    df = filter_df(df)

    for job_name in df.job_name.unique():
        print('>>', job_name)
        df_job_name = df[(df.job_name == job_name)]
        blocks_distribution_per_job(df_job_name, awd(job_name))


def mapred_failure_info(csv_path, awd):
    print("Failure info")
    df = read_failure_info_csv(csv_path)
    df = filter_df(df)

    print(df)



def mapred(input_dir, output_dir):

    awd = get_rel_path(output_dir)
    rwd = get_rel_path(input_dir)

    plotter_method = {
        'jobs.csv': mapred_jobs,
        'attempts.csv': mapred_attempts,
        'blocks.csv': blocks_distribution,
        'failure_info.csv': mapred_failure_info,
        'monitoring-dn.csv.gz': datanodes_monitoring,
        'max_map.csv': max_map_jobs,
        'map_runtimes_stats.csv': max_map_jobs,

        'exchanged_data.csv': mapred_exchanged_data,
        'jobs_hw.csv': mapred_all_hardware,
        'jobs_mr_sp.csv': mapred_mr_sp,
        'jobs_f.csv': mapred_failures,
        'ex_f.csv': mapred_failures_exchanged_data,
    }

    f_csv_path = rwd('failure_info.csv')
    if os.path.exists(f_csv_path):
        global df_failure_info
        df_failure_info = read_failure_info_csv(f_csv_path)
        mp = {9:10, 7:8}
        df_failure_info['datasize'] = df_failure_info['datasize'].apply(lambda x: x if not x in mp else mp[x])
        df_failure_info['input_size'] = df_failure_info['datasize']

    f_csv_path = rwd('jobs.csv')
    if os.path.exists(f_csv_path):
        global df_jobs
        df_jobs = read_mapred_csv(f_csv_path)
        df_jobs['datasize'] = df_jobs[['datasize', 'policy']].apply(reglage_datasize, axis=1)
        df_jobs = filter_df(df_jobs)
        if is_hibench_jobs(df_jobs):
            plot_hibench_jobs(df_jobs, awd, None)

    f_csv_path = rwd('attempts.csv')
    if os.path.exists(f_csv_path):
        global df_attempts
        df_attempts = read_attempts_csv(f_csv_path)

    for fname, pfunc in plotter_method.items():
        csv_path = rwd(fname)
        print(csv_path)
        if os.path.exists(csv_path):
            pfunc(csv_path, awd)


if __name__ == "__main__":

    if len(sys.argv) != 3:
        print('usage: ./plot_mapred.py input-dir output-dir')
        sys.exit(1)

    input_dir, output_dir = sys.argv[1:]

    df_jobs = None
    df_attempts = None
    df_failure_info = None
    df_kmeans = None

    mapred(input_dir, output_dir)
